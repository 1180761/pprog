/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;



import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ZDPessoa
 */
public class PedidoPrestacaoServicoTest {
    
    public Empresa emp = new Empresa();
    
    public PedidoPrestacaoServicoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of calculaCusto method, of class PedidoPrestacaoServico.
     */
    @Test
    public void testCalculaCusto() {
        System.out.println("calculaCusto");
        
        CodigoPostal codPostal1 = new CodigoPostal("4100-379", (float) 41.1496100 , (float) -8.6109900);
        CodigoPostal codPostal2 = new CodigoPostal("4100-379", (float) 38.0710968 , (float) -9.152138);
        
        EnderecoPostal endPostal = new EnderecoPostal(" ", codPostal1, " ");
        
        AreaGeografica areaGeo1 = new AreaGeografica("Porto", 10, 50000, codPostal1);
        AreaGeografica areaGeo2 = new AreaGeografica("Lisboa", 30, 50000, codPostal2);
        
        emp.getRegistoAreasGeograficas().addAreaGeografica(areaGeo1);
        emp.getRegistoAreasGeograficas().addAreaGeografica(areaGeo2);
        
        Categoria cat1 = new Categoria("cat1", "Descrição");
        
        ServicoLimitado serv1 = new ServicoLimitado("serv1", "Descricão", "Descrição Completa", 25, cat1);
        
        PedidoPrestacaoServico instance = new PedidoPrestacaoServico(new Cliente(), endPostal);
        
        instance.addPedidoServico(serv1, "Descrição", 120);
        instance.addPedidoServico(serv1, "Descrição", 60);
        
        double expResult = 85;
        double result = instance.calculaCusto();
        assertEquals(expResult, result, 0.0);
       
    }
    
}
