package com.mycompany.tp3;

import Autorizacao.AutorizacaoFacade;
import Autorizacao.model.PapelUtilizador;
import Autorizacao.model.SessaoUtilizador;
import UI.AtribuirPedidoPrestacaoUI;
import UI.EfetuarPedidoPrestacaoServicosUI;
import UI.IndicarDisponiblidadeDiariaUI;
import com.mycompany.sample.model.Configs;
import com.mycompany.sample.model.Empresa;
import com.mycompany.sample.model.TipoServico;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

/**
 *
 * @author jorgi
 */
public class main {

    public static Empresa emp;
    public static AutorizacaoFacade aut;
    public static List<TipoServico> ListaTiposServ;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {

        Properties props = Configs.getConfigVals();

        emp = Configs.criarEmpresa(props);

        ListaTiposServ = Configs.criaTiposServicosSuportados(props);
        emp.getAutorizacaoFacade().bootstrap();
        emp.getRegistoPedidosPrestacaoServicos().readPedidofromFile();
        emp.guardarinfo();
        showMenu();
    }

    public static void showMenu() throws IOException, FileNotFoundException, ClassNotFoundException {

        Formatter formatter = new Formatter(System.out);
        System.out.println("Nome Empresa: " + emp.getDesig() + " NIF: " + emp.getNif());

        Scanner scanner = new Scanner(System.in);
        final String menuContent = ("\nMenu\n"
                + "1 - Efetuar Pedido de Prestação de Serviços\n"
                + "2 - Indicar disponibilidade diária de prestação de serviços\n"
                + "3 - Afetar um prestador de serviço\n"
                + "0 - Terminar\n");
        String chr;
        String nome;
        String pass;
        SessaoUtilizador sessao;
        int check = 0;
        do {
            formatter.format("%s", menuContent);
            chr = scanner.nextLine();
            switch (chr) {
                case "1":
                    
                    System.out.println("Introduza o seu Id");
                    nome = scanner.nextLine();
                    System.out.println("Introduza a sua password");
                    pass = scanner.nextLine();
                    aut = emp.getAutorizacaoFacade();
                    sessao = aut.doLogin(nome, pass);
                    if (sessao == null) {
                        System.out.println("\nUtilizador não registado...");
                        break;
                    }

                    check = 0;
                    for (PapelUtilizador pu : sessao.getPapeisUtilizador()) {

                        if (pu.getPapel().compareTo("CLIENTE") == 0) {
                            EfetuarPedidoPrestacaoServicosUI ui = new EfetuarPedidoPrestacaoServicosUI();
                            ui.Pedido();
                            check = 1;
                            break;
                        }

                    }

                    if (check == 0) {
                        System.out.println("Utilizador Não Autorizado");
                    }

                    break;
                case "2":
                    
                    System.out.println("Introduza o seu Id");
                    nome = scanner.nextLine();
                    System.out.println("Introduza a sua password");
                    pass = scanner.nextLine();
                    aut = emp.getAutorizacaoFacade();
                    sessao = aut.doLogin(nome, pass);
                    if (sessao == null) {
                        System.out.println("\nUtilizador não registado...");
                        break;
                    }
                    
                    check = 0;
                    for (PapelUtilizador pu : sessao.getPapeisUtilizador()) {

                        if (pu.getPapel().compareTo("PRESTADOR SERVIÇO") == 0) {
                            IndicarDisponiblidadeDiariaUI ui = new IndicarDisponiblidadeDiariaUI();
                            ui.indicaDisponibilidadeController();
                            check = 1;
                            break;
                        }
                        
                        if (check == 0) {
                        System.out.println("\nUtilizador Não Autorizado");
                    }  
                    }
                    
                    break; 
                case "3":
                    
                    System.out.println("Introduza o seu Id");
                    nome = scanner.nextLine();
                    System.out.println("Introduza a sua password");
                    pass = scanner.nextLine();
                    aut = emp.getAutorizacaoFacade();
                    sessao = aut.doLogin(nome, pass);
                    if (sessao == null) {
                        System.out.println("\nUtilizador não registado...");
                        break;
                    }

                    check = 0;
                    for (PapelUtilizador pu : sessao.getPapeisUtilizador()) {

                        if (pu.getPapel().compareTo("ADMINISTRATIVO") == 0) {
                            AtribuirPedidoPrestacaoUI ui = new AtribuirPedidoPrestacaoUI();
                            ui.atribuirPedidoController();
                            check = 1;
                            break;
                        }

                    }

                    if (check == 0) {
                        System.out.println("\nUtilizador Não Autorizado");
                    }

                    break;
                case "0":
                    break;

                default:
                    formatter.format("Invalid option!");
            }
        } while (!"0".equals(chr));

    }
}
