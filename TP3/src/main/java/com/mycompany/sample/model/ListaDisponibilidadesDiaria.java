package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;

/** Lista de disponibilidades Diarias
 *
 */
public class ListaDisponibilidadesDiaria {

    private List<Disponibilidade> dispList;
    
    /** Construtor de uma nova lista de disponibilidades diarias
     * 
     */
    public ListaDisponibilidadesDiaria() {

        this.dispList = new ArrayList<Disponibilidade>();

    }

    public List<Disponibilidade> getListaDisp() {

        List<Disponibilidade> newDispList = new ArrayList<Disponibilidade>();

        newDispList = this.dispList;

        return newDispList;
    }
    /** Criar uma nova disponibilidade
     * 
     * @param dataInicio data de inicio da disponibilidade
     * @param horaInicio hora de inicio da disponibilidade
     * @param dataFim data do fim da disponibilidade
     * @param horaFim hora do fim da disponibilidade
     * @return nova disponibilidade
     */
    public Disponibilidade novoPedidoDisponibilidade(Data dataInicio, int horaInicio, Data dataFim, int horaFim) {

        Disponibilidade novaDisp = new Disponibilidade(dataInicio, horaInicio, dataFim, horaFim);
        
        return novaDisp;

    }

    public void addPeriodoDisponibilidade(Disponibilidade novaDisp) {

        this.dispList.add(novaDisp);

    }
    
    /** Registar um periodo de disponibilidade
     * 
     * @param novaDisp disponibilidade a ser registada
     */
    public void registaPeriodoDisponibilidade(Disponibilidade novaDisp) {
        if (validaPeriodoDisponibilidade(novaDisp)) {
            addPeriodoDisponibilidade(novaDisp);
        }
    }
    /** Validar disponibilidade
     * 
     * @param novaDisp disponibilidade a ser validada
     * @return se é valida ou não
     */
    
    public boolean validaPeriodoDisponibilidade(Disponibilidade novaDisp) {
        if (0 > novaDisp.getHoraI()&& 24 < novaDisp.getHoraI()) {
            return false;
        }
        if (0 > novaDisp.getHoraF() && 24 < novaDisp.getHoraF()) {
            return false;
        }
        return true;
    }

}
