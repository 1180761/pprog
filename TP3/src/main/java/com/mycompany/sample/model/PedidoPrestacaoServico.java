package com.mycompany.sample.model;

import static com.mycompany.tp3.main.emp;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/** Pedidos de prestacao de servicos da empresa.
 *
 */
public class PedidoPrestacaoServico implements Serializable{

    private int numero;

    private Data Data;

    private double total;

    private Cliente cliente;

    private EnderecoPostal endPostal;

    private OutroCusto oCusto;

    private List<DescricaoServicoPedido> DescServList = new ArrayList<DescricaoServicoPedido>();

    private List<PreferenciaHorario> PrefHorList = new ArrayList<PreferenciaHorario>();

    private List<OutroCusto> OutroCustoList = new ArrayList<OutroCusto>();

    private static final int NUMERO_POR_OMISSAO = 0;

    private static final Data DATA_POR_OMISSAO = new Data();

    private static final float TOTAL_POR_OMISSAO = 0;

    private static final Cliente CLIENTE_POR_OMISSAO = new Cliente();

    private static final EnderecoPostal END_P_POR_OMISSAO = new EnderecoPostal();
    
    /** Construtor de um pedido de prestacao de servico
     * 
     * @param cliente cliente que efetua pedido
     * @param endPostal endereco postal do cliente
     */
    public PedidoPrestacaoServico(Cliente cliente, EnderecoPostal endPostal) {

        if ((cliente == null) || (endPostal == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = cliente;
        this.endPostal = endPostal;

    }
    
    /** Construtor de um pedido de prestacao de servico por omissao de dados
     * 
     */

    public PedidoPrestacaoServico() {

        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = CLIENTE_POR_OMISSAO;
        this.endPostal = END_P_POR_OMISSAO;

    }

    public Cliente getCliente() {

        return this.cliente;
    }

    public int getNumero() {

        return this.numero;
    }
    
    public EnderecoPostal getEndPostal(){
        
        return this.endPostal;
    }
    
    public Data getData(){
        
        return this.Data;
    }
    
    public void setData(Data data){
        this.Data = data;
    }
    
    public List<PreferenciaHorario> getListPreferenciaHorario(){
        
        List<PreferenciaHorario> newPreHorList = new ArrayList<PreferenciaHorario>();
        
        newPreHorList = this.PrefHorList;
        
        return newPreHorList;
    }
    
    public List<DescricaoServicoPedido> getListDescricaoServicoPedido(){
        
        List<DescricaoServicoPedido> newDescServList = new ArrayList<DescricaoServicoPedido>();
        
        newDescServList = this.DescServList;
        
        return newDescServList;  
    }
    
    public int getDuracaoTotal(){
        
        int duracao = 0;
        
        for(DescricaoServicoPedido d: this.getListDescricaoServicoPedido()){
            
            duracao += d.getDuracao();
            
        }
        
        return duracao;
    }

    public void setNumero(int numero) {

        this.numero = numero;

    }
    /** Adiciona pedido de prestacao de servico
     * 
     * @param serv servico do pedido
     * @param desc descricao do servico
     * @param dur duracao do servico
     */
    public void addPedidoServico(Servico serv, String desc, int dur) {

        DescricaoServicoPedido novoPedido = new DescricaoServicoPedido(serv, desc, dur);

        this.DescServList.add(novoPedido);

    }

    /** Adiciona um horario ao pedido
     * 
     * @param Data data para realizacao do pedido
     * @param hora hora para realizacao do pedido
     */
    public void addHorario(Data Data, int hora) {

        int ordem = countHorarios();

        PreferenciaHorario newHorario = new PreferenciaHorario(ordem, Data, hora);

        this.PrefHorList.add(newHorario);

    }
    
    /** Contar o numero de horarios do pedido
     * 
     * @return numero de horarios
     */

    private int countHorarios() {

        int ordem = 0;

        for (PreferenciaHorario h : this.PrefHorList) {
            ordem = h.getOrdem();
        }

        return ordem + 1;

    }

    public DescricaoServicoPedido getDSP(int i) {

        for (int j = 0; j < this.DescServList.size(); j++) {

            if (j == i) {
                return this.DescServList.get(j);
            }

        }

        return null;

    }
    /** Calcular custo total
     * 
     * @return custo total
     */
    public double getCustoTotal() {
        double c = calculaCusto();
        return c;
    }
    
    /** Calcular custo
     * 
     * @return custo
     */
    public double calculaCusto() {

        double custo = 0;

        for (DescricaoServicoPedido dp : this.DescServList) {
            custo += dp.getCusto();
        }

        this.OutroCustoList.clear();

        AreaGeografica areageo = RegistoAreasGeograficas.getAreaGeograficaMaisPerto(this.endPostal.getCodPostal());

        double custoDeslocacao = areageo.getCusto();

        oCusto = new OutroCusto(areageo.getDesig(), custoDeslocacao);

        this.OutroCustoList.add(oCusto);

        custo += custoDeslocacao;

        this.total = custo;

        return custo;
    }
   /** Validar pedido
    * 
    * @return se é ou não valido
    */
    public boolean validaPedido() {

        if ((this.cliente == null) || (this.endPostal == null) || (this.Data == null) || (this.numero <= 0) || (this.total <= 0)
                || (this.DescServList == null) || (this.OutroCustoList == null) || (this.PrefHorList == null)
                || (this.Data == DATA_POR_OMISSAO) || (this.endPostal == END_P_POR_OMISSAO) || (this.cliente == CLIENTE_POR_OMISSAO)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio...");
        }

        return true;
    }
    
    @Override
    public String toString(){
        System.out.println("Descricao do Servico Pedido:");
                System.out.println("Descricao - duracao - codigo servico - descricao - descricao completa - custo por hora - codigo categoria - descricao da categoria");
        for(DescricaoServicoPedido d : DescServList){
            System.out.println(d.toString());
        }
        System.out.println("\nPreferencia de horarios:");
        System.out.println("Ordem - Data - Hora");
        for(PreferenciaHorario pref : PrefHorList){
            System.out.println(pref.toString());
        }
        return String.format("Custo total : %.2f", this.total);
    }
    
   
}
