package com.mycompany.sample.model;

/** Disponibilidade do prestador de servico.
 *
 */
public class Disponibilidade {
    
    private Data dataInicio;
    
    private int horaInicio;
    
    private Data dataFim;
    
    private int horaFim;
    
    private static final Data DATA_POR_OMISSAO = new Data();
    
    private static final int HORA_POR_OMISSAO = 0;
    
    /** Construtor de disponibilidade de um prestador de servico.
     * 
     * @param dataInicio data de inicio da disponibilidade
     * @param horaInicio hora de inicio da disponibilidade
     * @param dataFim data de fim da disponibilidade
     * @param horaFim hora de fim da disponibilidade
     */
    public Disponibilidade(Data dataInicio, int horaInicio, Data dataFim, int horaFim){
        
        this.dataFim = dataFim;
        this.dataInicio = dataInicio;
        this.horaFim = horaFim;
        this.horaInicio = horaInicio;
        
    }
    
    /** Construtor de disponibilidade com omissao de dados.
     * 
     */
    public Disponibilidade(){
        
        this.dataFim = DATA_POR_OMISSAO;
        this.dataInicio = DATA_POR_OMISSAO;
        this.horaFim = HORA_POR_OMISSAO;
        this.horaInicio = HORA_POR_OMISSAO;
        
    }
    
    public Data getDataInicio(){
        
        return this.dataInicio;
    }
    
    public Data getDataFim(){
        
        return this.dataFim;
    }
    
    public int getHoraI(){
        
        return this.horaInicio;
    }
    
    public int getHoraF(){
        
        return this.horaFim;
    }
    
    @Override
    public String toString(){
        return String.format("%s - %d - %s - %d", this.dataInicio.toString() ,this.horaInicio , this.dataFim.toString(), this.horaFim);
    }
    
}
