package com.mycompany.sample.model;

import java.io.Serializable;

/** Preferencia de horario de pedido.
 *
 */
public class PreferenciaHorario implements Serializable {

    private int ordem;

    private Data data;

    private int hora;
    
    /** Construtor de preferencia de horario de pedido.
     * 
     * @param ordem ordem do da preferencia de horario
     * @param data data da preferencia de horario
     * @param hora hora de preferencia de horario
     */
    public PreferenciaHorario(int ordem, Data data, int hora) {

        this.ordem = ordem;
        this.data = data;
        this.hora = hora;

    }

    public int getOrdem() {

        return this.ordem;
    }

    public Data getData() {
        return data;
    }

    public int getHora() {
        return hora;
    }
    
    @Override
    public String toString(){
        return String.format("%d - %s - %d", this.ordem,this.data.toString(), this.hora);
    }
}
