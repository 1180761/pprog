package com.mycompany.sample.model;

import java.util.Comparator;

/** Comparador de distancias.
 * 
 */
public class ComparatorDistancia implements Comparator{
    
    private CodigoPostal codPostalServ;
    
    public ComparatorDistancia(PedidoPrestacaoServico pedServ){
        
        this.codPostalServ = pedServ.getEndPostal().getCodPostal();
        
    }
    /** Obter area mais perto de um servico.
     * 
     * @param prestadorServ prestador de servico com areas geograficas.
     * @return area mais perto do servico
     */
    private double areaMaisPertoServ(PrestadorServico prestadorServ){
        
        double dist = 0;
        double distAux = 0;
        
        for(AreaGeografica a : prestadorServ.getListAreaGeografica()){
            
            distAux = RegistoAreasGeograficas.distancia(this.codPostalServ.getLat(), this.codPostalServ.getLog(), a.getCodPostal().getLat(), a.getCodPostal().getLog());
            
            if( dist < distAux )
                dist = distAux;
            
        }
        
        return dist;
    }
    
    @Override
    public int compare(Object obj1, Object obj2){
        
        PrestadorServico p1 = (PrestadorServico) obj1;
        PrestadorServico p2 = (PrestadorServico) obj2;
        
        return (int) (this.areaMaisPertoServ(p1) - this.areaMaisPertoServ(p2));
        
      
    }
    
    
    
    
}
