package com.mycompany.sample.model;

import static com.mycompany.tp3.main.emp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jorgi
 */
public class RegistoClientes {

    private final Set<Cliente> m_lstClientes = new HashSet<>();

    public Cliente getClienteByEmail(String strEMail) {
        
        for (Cliente cliente : this.m_lstClientes) {
            if (cliente.hasEmail(strEMail)) {
                return cliente;
            }
        }

        return null;
    }
    /** Criar cliente
     * 
     * @param strNome nome do cliente
     * @param strNIF nif do cliente
     * @param strTelefone telefone do cliente
     * @param strEmail email do cliente
     * @param morada morada do cliente
     * @return 
     */
    public Cliente novoCliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal morada) {
        return new Cliente(strNome, strNIF, strTelefone, strEmail, morada);
    }

    /** Registar cliente
     * 
     * @param oCliente cliente a ser registado
     * @param strPwd password do cliente
     * @return 
     */
    public boolean registaCliente(Cliente oCliente, String strPwd) {
        if (this.validaCliente(oCliente, strPwd)) {
            if (emp.getAutorizacaoFacade().registaUtilizadorComPapel(oCliente.getNome(), oCliente.getEmail(), strPwd, Constantes.PAPEL_CLIENTE)
            )
                return addCliente(oCliente);
        }
        return false;
    }

    /** Adiciona cliente
     * 
     * @param oCliente cliente a ser adicionado
     * @return 
     */
    public boolean addCliente(Cliente oCliente) {
        return m_lstClientes.add(oCliente);
    }

    /** valida cliente
     * 
     * @param oCliente cliente a ser validado
     * @param strPwd password do cliente
     * @return 
     */
    public boolean validaCliente(Cliente oCliente, String strPwd) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (emp.getAutorizacaoFacade().existeUtilizador(oCliente.getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        //

        return bRet;
    }

    public List<Cliente> getListaClientes() {
        List<Cliente> lc = new ArrayList<>();
        lc.addAll(this.m_lstClientes);
        return lc;
    }

}
