package com.mycompany.sample.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Registo de categorias da Empresa.
 *
 */
public class RegistoCategorias {

    private final Set<Categoria> m_lstCategorias = new HashSet<>();

    public Categoria getCategoriaById(String strId) {
        for (Categoria cat : this.m_lstCategorias) {
            if (cat.hasId(strId)) {
                return cat;
            }
        }

        return null;
    }
    /** Criar nova categoria
     * 
     * @param strCodigo codigo da categoria
     * @param strDescricao descricao da categoria
     * @return 
     */
    public Categoria novaCategoria(String strCodigo, String strDescricao) {
        return new Categoria(strCodigo, strDescricao);
    }

    /** Regista categoria
     * 
     * @param oCategoria categoria a ser registada
     * @return 
     */
    public boolean registaCategoria(Categoria oCategoria) {
        if (this.validaCategoria(oCategoria)) {
            return addCategoria(oCategoria);
        }
        return false;
    }
    /** Adiciona categoria
     * 
     * @param oCategoria categoria a ser adicionada
     * @return 
     */
    public boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }
    /** Valida Categoria
     * 
     * @param oCategoria cateogira a ser validada
     * @return 
     */
    public boolean validaCategoria(Categoria oCategoria) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<Categoria> getCategorias() {
        List<Categoria> lc = new ArrayList<>();
        lc.addAll(this.m_lstCategorias);
        return lc;
    }

}
