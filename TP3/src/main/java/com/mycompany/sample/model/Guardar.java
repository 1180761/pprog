/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import static com.mycompany.tp3.main.ListaTiposServ;
import static com.mycompany.tp3.main.emp;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Guardar {

    public void guardaFicheiros() throws IOException {
        carregaFicheiroClientes();
        carregaFicheiroCategorias();
        carregaFicheiroServicos();
        carregaFicheiroAreasGeograficas();
        carregaFicheiroPrestadorServico();
        carregaFicheiroEnderecos();
                
    }

    public String getPwd(String linha) {
        String atributos[] = linha.trim().split(";");
        return atributos[4];
    }

    public Cliente novoCliente(String linha) {
        String atributos[] = linha.trim().split(";");
        CodigoPostal codPostal = new CodigoPostal(atributos[7], Float.valueOf(atributos[8]), Float.valueOf(atributos[9]));
        EnderecoPostal end = new EnderecoPostal(atributos[5], codPostal, atributos[6]);
        Cliente c = new Cliente(atributos[0], atributos[1], atributos[2], atributos[3], end);
        return c;
    }

    public void carregaFicheiroClientes() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("clientes.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            emp.getRegistoClientes().registaCliente(novoCliente(linha), getPwd(linha));
        }
        br.close();
    }

    public Servico novoServico(String linha) throws IOException {
        String atributos[] = linha.split(";");
        Categoria cat = emp.getRegistoCategorias().getCategoriaById(atributos[5]);

        TipoServico tp = new TipoServico();

        for (TipoServico t : ListaTiposServ) {

            if (atributos[4].compareTo(t.getDesignacao()) == 0) {
                tp = t;
                break;
            }

        }

        if (atributos[6].compareTo(" ") == 0) {

            Servico serv = tp.novoServico(atributos[0], atributos[1], atributos[2], Double.valueOf(atributos[3]), cat);
            return serv;

        } else {
            emp.addListaCodServDuracao(atributos[0]);
            System.out.println(emp.getListaCodServDuracao());
            Servico serv = tp.novoServicoD(atributos[0], atributos[1], atributos[2], Double.valueOf(atributos[3]), cat, Integer.valueOf(atributos[6]));
            return serv;
        }

    }

    public void carregaFicheiroServicos() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("servicos.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            Servico serv = novoServico(linha);
            emp.getRegistoServicos().registaServico(serv);
        }
        br.close();
    }

    public Categoria novaCategoria(String linha) {
        String atributos[] = linha.split(";");
        Categoria cat = new Categoria(atributos[0], atributos[1]);
        return cat;
    }

    public void carregaFicheiroCategorias() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("categorias.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            Categoria cat = novaCategoria(linha);
            emp.getRegistoCategorias().registaCategoria(cat);
        }
        br.close();
    }

    public AreaGeografica novaAreaGeografica(String linha) {
        String atributos[] = linha.split(";");
        CodigoPostal codPostal = new CodigoPostal(atributos[3], Float.valueOf(atributos[4]), Float.valueOf(atributos[5]));
        AreaGeografica areageo = new AreaGeografica(atributos[0], Double.valueOf(atributos[1]), Double.valueOf(atributos[2]), codPostal);
        return areageo;
    }

    public void carregaFicheiroAreasGeograficas() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("areasgeograficas.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            AreaGeografica areageo = novaAreaGeografica(linha);
            emp.getRegistoAreasGeograficas().registaAreaGeografica(areageo);
        }
        br.close();
    }

    public PrestadorServico novoPrestadorServico(String linha) {
        String atributos[] = linha.split(";");
        AreaGeografica areageo = emp.getRegistoAreasGeograficas().getAreaGeograficaByDesig(atributos[5]);
        Categoria cat = emp.getRegistoCategorias().getCategoriaById(atributos[6]);
        PrestadorServico prest = new PrestadorServico(Integer.valueOf(atributos[0]), atributos[1], atributos[2], atributos[3], areageo, cat);
        return prest;
    }

    public void carregaFicheiroPrestadorServico() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("prestadorservicos.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            PrestadorServico prest = novoPrestadorServico(linha);
            emp.getRegistoPrestadorServicos().registaPrestadorServico(prest, getPwd(linha));
        }
        br.close();
    }
    
    public EnderecoPostal novoEnderecoPostal(String linha){
        String atributos[] = linha.split(";");
        CodigoPostal cod = new CodigoPostal(atributos[2], Float.valueOf(atributos[3]), Float.valueOf(atributos[4]));
        EnderecoPostal end = new EnderecoPostal(atributos[1], cod, atributos[5]);
        return end;
    }

    public void carregaFicheiroEnderecos() throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("enderecos.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            EnderecoPostal end = novoEnderecoPostal(linha);
            getCliente(linha).addEnderecoPostal(end);
        }
        br.close();
    }
    
    public Cliente getCliente(String linha){
        String atributos[] = linha.split(";");
        Cliente cli = emp.getRegistoClientes().getClienteByEmail(atributos[0]);
        return cli;
    }

}
