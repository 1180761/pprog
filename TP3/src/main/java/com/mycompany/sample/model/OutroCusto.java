package com.mycompany.sample.model;

import java.io.Serializable;

/** Outro custo
 *
 */
public class OutroCusto implements Serializable {
    
    private String designacao;
    private double valor;
    
    private static final String DESIG_POR_OMISSAO = "SEM DESIGNACAO";
    private static final double VALOR_POR_OMISSAO = 0;
    
    /** Construtor de um outro custo
     * 
     * @param designacao designacao do outro custo
     * @param valor valor do outro custo
     */
    public OutroCusto(String designacao, double valor){
        
        this.designacao = designacao;
        this.valor = valor;
        
    }
    /** Construtor de um outro custo por omissao de dados
     * 
     */
    public OutroCusto(){
        
        this.designacao = DESIG_POR_OMISSAO;
        this.valor = VALOR_POR_OMISSAO;
        
    }
    
}
