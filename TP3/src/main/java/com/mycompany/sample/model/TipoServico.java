/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.lang.reflect.Constructor;


/**
 *
 * @author ZDPessoa
 */
public class TipoServico  {
    
    private String designacao;
    private String classeServico;
    
    public TipoServico(String desig, String cServ){
        
        this.designacao = desig;
        this.classeServico = cServ;
        
    }
    
    public TipoServico(){
        
        this.designacao = " ";
        this.classeServico = " ";
        
    }
    
    public String getDesignacao(){
        
        return this.designacao;
    }
    
    public String getclasseServico(){
        return this.classeServico;
    }
    
    public Servico novoServico(String id, String descB, String descC, double custo, Categoria cat) {
        
    try{
    Class<?> oClass = Class.forName(this.classeServico);
    Class argsClasses[] = new Class[] { String.class, String.class, String.class, double.class, Categoria.class};
    Constructor construct = oClass.getConstructor(argsClasses);
    Object[] argsValues = new Object[] { id, descB, descC, custo, cat };
    Servico serv = (Servico) construct.newInstance(argsValues);
    return serv;
    
    }catch (Exception e){
        System.out.println("Something went wrong...");
        return null;
        }
     
    }
    
    public Servico novoServicoD(String id, String descB, String descC, double custo, Categoria cat, int duracao) {
        
    try{
    Class<?> oClass = Class.forName(this.classeServico);
    Class argsClasses[] = new Class[] { String.class, String.class, String.class, double.class, Categoria.class, int.class};
    Constructor construct = oClass.getConstructor(argsClasses);
    Object[] argsValues = new Object[] { id, descB, descC, custo, cat, duracao};
    Servico serv = (Servico) construct.newInstance(argsValues);
    return serv;
    
    }catch (Exception e){
        System.out.println("Something went wrong...");
        return null;
        }
     
    }
    
    
}
