package com.mycompany.sample.model;

import java.io.Serializable;

/** Codigo Postal
 *
 *
 */
public class CodigoPostal implements Serializable {
    
    private final String codPostal;
    private final float latitude;
    private final float longitude;
    
    private static final String COD_POSTAL_POR_OMISSAO = "SEM COD_POSTAL";
    private static final float LAT_POR_OMISSAO = 0;
    private static final float LOG_POR_OMISSAO = 0;
    
    /** Construtor do codigo postal.
     * 
     * @param codPostal - codigo postal
     * @param latitude - latitude do codigo postal
     * @param longitude - longitude do codigo postal
     */
    public CodigoPostal(String codPostal, float latitude, float longitude){
        
        this.codPostal = codPostal;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    /** Construtor do codigo postal por omissao de dados.
     * 
     */
    public CodigoPostal(){
        
        this.codPostal = COD_POSTAL_POR_OMISSAO;
        this.latitude = LAT_POR_OMISSAO;
        this.longitude = LOG_POR_OMISSAO;
        
    }
    
    public float getLat(){
        
        return this.latitude;
    }
    
    public float getLog(){
        
        return this.longitude;
    }
    
    public String getCodPostal(){
        return this.codPostal;
    }
    
}
