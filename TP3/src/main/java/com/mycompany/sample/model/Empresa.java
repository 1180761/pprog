/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import Autorizacao.AutorizacaoFacade;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/** Empresa
 * 
 */

public class Empresa {

    private String m_strDesignacao;
    private String m_strNIF;
    private final AutorizacaoFacade m_oAutorizacao;
    private final RegistoAreasGeograficas m_oRegistoAreasGeogras;
    private final RegistoCategorias m_oRegistoCategorias;
    private final RegistoClientes m_oRegistoClientes;
    private final RegistoPrestadorServicos m_oRegistoPrestadorServicos;
    private final RegistoServicos m_oRegistoServicos;
    private final RegistoPedidosPrestacaoServicos m_oRegistoPedidosPrestacaoServicos;
    private final Guardar guardar;
    private List<String> ListaCodServDuracao = new ArrayList<String>();

    private static final String DESIG_POR_OMISSAO = "SEM DESIGNACAO";
    private static final String NIF_POR_OMISSAO = "SEM NIF";
    
    /** Construtor de Empresa
     * 
     * @param strDesignacao designacao da empresa
     * @param strNIF nif da empresa
     */
    public Empresa(String strDesignacao, String strNIF) {
        if ((strDesignacao == null) || (strNIF == null)
                || (strDesignacao.isEmpty()) || (strNIF.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;
        this.m_strNIF = strNIF;
        this.m_oRegistoAreasGeogras = new RegistoAreasGeograficas();
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_oRegistoCategorias = new RegistoCategorias();
        this.m_oRegistoClientes = new RegistoClientes();
        this.m_oRegistoPrestadorServicos = new RegistoPrestadorServicos();
        this.m_oRegistoServicos = new RegistoServicos();
        this.m_oRegistoPedidosPrestacaoServicos = new RegistoPedidosPrestacaoServicos();
        this.guardar = new Guardar();
    }
    
    /** Construtor da empresa por omissao de dados
     * 
     */
    public Empresa(){
        
        this.m_strDesignacao = DESIG_POR_OMISSAO;
        this.m_strNIF = NIF_POR_OMISSAO;
        this.m_oRegistoAreasGeogras = new RegistoAreasGeograficas();
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_oRegistoCategorias = new RegistoCategorias();
        this.m_oRegistoClientes = new RegistoClientes();
        this.m_oRegistoPrestadorServicos = new RegistoPrestadorServicos();
        this.m_oRegistoServicos = new RegistoServicos();
        this.m_oRegistoPedidosPrestacaoServicos = new RegistoPedidosPrestacaoServicos();
        this.guardar = new Guardar();
        
    }
    
    public void addListaCodServDuracao(String codServ){
        
        this.ListaCodServDuracao.add(codServ);
    }
    
    public List<String> getListaCodServDuracao(){
        
        List<String> newList = new ArrayList<String>(); 
     
        newList = this.ListaCodServDuracao;
        
        return newList;
    }

    public String getDesig(){
        
        return this.m_strDesignacao;
    }
    
    public String getNif(){
        
        return this.m_strNIF;
    }
    
    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    public RegistoAreasGeograficas getRegistoAreasGeograficas() {
        return this.m_oRegistoAreasGeogras;
    }
    
    public RegistoClientes getRegistoClientes(){
        return this.m_oRegistoClientes;
    }
    
    public RegistoCategorias getRegistoCategorias(){
        return this.m_oRegistoCategorias;
    }
    
    public RegistoPrestadorServicos getRegistoPrestadorServicos(){
        return this.m_oRegistoPrestadorServicos;
    }
    
    public RegistoServicos getRegistoServicos(){
        return this.m_oRegistoServicos;
    }
    
    public RegistoPedidosPrestacaoServicos getRegistoPedidosPrestacaoServicos(){
        return this.m_oRegistoPedidosPrestacaoServicos;
    }
    
    public void guardarinfo() throws IOException{
        guardar.guardaFicheiros();
    }
}
