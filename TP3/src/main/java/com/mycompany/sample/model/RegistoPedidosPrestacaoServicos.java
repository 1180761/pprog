package com.mycompany.sample.model;

import static com.mycompany.tp3.main.emp;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Registo Pedidos Prestacao Servicos da Empresa.
 *
 */
public class RegistoPedidosPrestacaoServicos implements Serializable {

    private final Set<PedidoPrestacaoServico> m_lstPedidosPrestacaoServico = new HashSet<>();

    /**
     * Criar pedido
     *
     * @param cli cliente a fazer pedido
     * @param endPostal endereco postal do cliente
     * @return
     */
    public PedidoPrestacaoServico novoPedido(Cliente cli, EnderecoPostal endPostal) {

        PedidoPrestacaoServico novoPedPrestacaoServ = new PedidoPrestacaoServico(cli, endPostal);

        return novoPedPrestacaoServ;

    }

    /**
     * Registar cliente
     *
     * @param pedPrestacaoServ pedido a ser registado
     * @return
     * @throws IOException
     */

    public int registaPedido(PedidoPrestacaoServico pedPrestacaoServ) throws IOException {

        int numero = this.geraNumeroPedido();

        pedPrestacaoServ.setNumero(numero);

        this.addPedido(pedPrestacaoServ);

        this.notificaCliente(pedPrestacaoServ);

        addPedidotoFile(pedPrestacaoServ);

        return numero;

    }

    public List<PedidoPrestacaoServico> getListPedidoPrestacaoServico() {

        List<PedidoPrestacaoServico> newListaPedidos = new ArrayList<PedidoPrestacaoServico>();

        newListaPedidos.addAll(this.m_lstPedidosPrestacaoServico);

        return newListaPedidos;

    }

    /**
     * Remover pedido
     *
     * @param ped pedido a ser removido
     * @return
     */
    public boolean removePedidoPrestacao(PedidoPrestacaoServico ped) {

        return this.m_lstPedidosPrestacaoServico.remove(ped);

    }

    /**
     * Adicionar pedido
     *
     * @param novoPedPrestacaoServ pedido a ser adicionado
     */
    private void addPedido(PedidoPrestacaoServico novoPedPrestacaoServ) {

        this.m_lstPedidosPrestacaoServico.add(novoPedPrestacaoServ);

    }

    /**
     * Gera numero do pedido
     *
     * @return
     */
    private int geraNumeroPedido() {

        int numero = 0;

        for (PedidoPrestacaoServico p : this.m_lstPedidosPrestacaoServico) {
            numero = p.getNumero();
        }

        return numero + 1;

    }

    /**
     * Notifica cliente
     *
     * @param pedPrestacaoServ pedido
     */
    private void notificaCliente(PedidoPrestacaoServico pedPrestacaoServ) {

        pedPrestacaoServ.getCliente().getEmail();

        // SEND EMAIL
    }
    /** Adiciona pedido ao ficheiro
     * 
     * @param pedPrestacaoServ pedido a ser adicionado
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private void addPedidotoFile(PedidoPrestacaoServico pedPrestacaoServ) throws FileNotFoundException, IOException {
        String filename = "pedidos.bin";
        FileOutputStream fileout = new FileOutputStream(filename);
        ObjectOutputStream in = new ObjectOutputStream(fileout);
        in.writeObject(pedPrestacaoServ);
        in.close();
        fileout.close();
    }
    /** Lê ficheiro com pedidos
     * 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public void readPedidofromFile() throws FileNotFoundException, IOException, ClassNotFoundException {
        String filename = "pedidos.bin";
        FileInputStream fileout = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(fileout);
        PedidoPrestacaoServico ped = (PedidoPrestacaoServico) in.readObject();
        emp.getRegistoPedidosPrestacaoServicos().registaPedido(ped);
        in.close();
        fileout.close();
    }
}
