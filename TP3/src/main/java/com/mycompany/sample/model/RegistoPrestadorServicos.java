/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import static com.mycompany.tp3.main.emp;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/** Registo Prestador de Servicos da Empresa
 *
 */
public class RegistoPrestadorServicos {

    private final Set<PrestadorServico> m_lstPrestadoresServicos = new HashSet<>();

    public PrestadorServico getPrestadorServicos(String email) {

        for (PrestadorServico ps : this.m_lstPrestadoresServicos) {
            if (ps.getEmail().compareTo(email) == 0) {
                return ps;
            }
        }

        return null;
    }

    public boolean registaPrestadorServico(PrestadorServico prest, String psw) {
        if (this.validaPrestadorServico(prest, psw)) {
            if (emp.getAutorizacaoFacade().registaUtilizadorComPapel(prest.getNome(), prest.getEmail(), psw, Constantes.PAPEL_PRESTADOR_SERVICO)
            )
                return addPrestadorServico(prest);
        }
        return false;
    }

    public boolean addPrestadorServico(PrestadorServico prest) {
        return m_lstPrestadoresServicos.add(prest);
    }

    public boolean validaPrestadorServico(PrestadorServico prest, String psw) {
        boolean bRet = true;
        //valida
        return bRet;
    }

    public List<PrestadorServico> getPrestadorServicos() {
        List<PrestadorServico> lc = new ArrayList<>();
        lc.addAll(this.m_lstPrestadoresServicos);
        return lc;
    }
    
    /**
     * Cria uma Lista de prestadores ordenada em relação á distância ao local de atuação de um Pedido de Prestação de Servico
     * Esta lista contêm apenas Prestadores que apresentam compatibilidade de horário com o Pedido
     * @param ped : Pedido de Prestação de Servico
     * @return Devolve a Lista de Prestadores de serviço disponiveis para o Pedido recebeido ordenada em relação a distância
     */
    public List<PrestadorServico> sortPrestadorPedido(PedidoPrestacaoServico ped){
        
        List<PrestadorServico> prestadorList = new ArrayList<PrestadorServico>();
        
        prestadorList = this.getPrestadorServicos();    //Cria uma cópia da Lista de Prestadores Exixtentes
        
        Collections.sort(prestadorList, new ComparatorDistancia(ped)); //Organiza a Lista em relação á distânica do código postal do Pedido de Servico
        
        int duracao = ped.getDuracaoTotal(); //recebe a duração total do pedido
        
        int check = 0;
        double dist = 0;
        double distAux = 0;
        int i = 0;
        AreaGeografica areaGeo = new AreaGeografica();
        
        for(PrestadorServico ps : m_lstPrestadoresServicos){
            check = 0;
            dist = 0;
            distAux = 0;
            i = 0;
            //Verifica se o prestador está disponivel em pelo menos uma preferencia de horário do Pedido
            for( PreferenciaHorario ph : ped.getListPreferenciaHorario()){
                if(!(ps.isAvailable(ph.getData(), ph.getHora(), duracao))){
                    check = 1;
                    break;
                }
            }
            
            if(check != 1){
            for(AreaGeografica a : ps.getListAreaGeografica()){
      
            distAux = RegistoAreasGeograficas.distancia(ped.getEndPostal().getCodPostal().getLat(), ped.getEndPostal().getCodPostal().getLog(), a.getCodPostal().getLat(), a.getCodPostal().getLog());
            
                if( dist < distAux || i == 0){
                    dist = distAux;
                    areaGeo = a;
                    i = 1;
                }
                        
            }
            if(areaGeo.getRaio() < dist){
                        check = 1;
                    }
            }
            
            //Se o prestador não estiver disponivel é removido da lista
            if(check == 1){
                prestadorList.remove(ps);
            }
            
        }
        
        return prestadorList;
    }

}
