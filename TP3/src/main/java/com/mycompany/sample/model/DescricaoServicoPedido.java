package com.mycompany.sample.model;

import java.io.Serializable;

/** Descricao do Servico Pedido
 * 
 */
public class DescricaoServicoPedido implements Serializable{
    
    private String descricao;
    
    private int duracao;
    
    private Servico serv;
    
    /** Construtor do descricao do Servico Pedido.
     * 
     * @param serv servico
     * @param descricao descricao do servico
     * @param duracao duracao do servico
     */
    public DescricaoServicoPedido(Servico serv, String descricao, int duracao){
        
        this.descricao = descricao;
        this.duracao = duracao;
        this.serv = serv;
    }
    
    public double getCusto(){
        
        return this.serv.getCustoParaDuracao(duracao);
    }
    
    public int getDuracao(){
        
        return this.duracao;
    }
    
    @Override
    public String toString(){
       return String.format("%s - %d - %s", this.descricao, this.duracao, serv.toString());
    }
}
