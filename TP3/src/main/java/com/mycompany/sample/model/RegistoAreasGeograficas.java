package com.mycompany.sample.model;

import java.util.HashSet;
import java.util.Set;

/** Registo de areas geografica da Empresa.
 *
 */
public class RegistoAreasGeograficas {

    private static final Set<AreaGeografica> m_lstAreasGeograficas = new HashSet<>();
    
    /** Regista uma area geografica
     * 
     * @param areageo area geografica a ser registada
     * @return se foi ou nao registada
     */
    public boolean registaAreaGeografica(AreaGeografica areageo) {
        if (this.validaAreaGeografica(areageo)) {
            return addAreaGeografica(areageo);
        }
        return false;
    }
    /** Adicionar area geografica a empresa
     * 
     * @param areageo area geografica a ser adicionada
     * @return se foi ou nao adicionada
     */
    public boolean addAreaGeografica(AreaGeografica areageo) {
        return m_lstAreasGeograficas.add(areageo);
    }
    /** Valida area geografica
     * 
     * @param areageo area geografica a ser validada
     * @return se é ou nao valida
     */
    public boolean validaAreaGeografica(AreaGeografica areageo) {
        // valida
        return true;
    }
    /** Obter area geografica mais proxima
     * 
     * @param codPostal codigo postal de servico
     * @return area geografica mais proxima
     */
     public static  AreaGeografica getAreaGeograficaMaisPerto(CodigoPostal codPostal){
        
        AreaGeografica areaGeo = new AreaGeografica();
        
        double dist = 0;
        double distAux = 0;
        
        int i = 0;
        
        for (AreaGeografica a : m_lstAreasGeograficas){
            
            dist = distancia(a.getCodPostal().getLat(), a.getCodPostal().getLog(), 
                    codPostal.getLat(), codPostal.getLog());
              
            if(dist < distAux || i == 0){
                
               areaGeo = a;
                
               distAux = dist;
               
               i++;
            }
            
        }
        
        return areaGeo;
    }
     /** Calcular distancia entre codigos postais.
      * 
      * @param lat1 latitude da primeiro codigo postal
      * @param lon1 longitude do primeiro codigo postal
      * @param lat2 latitude do segundo condigo postal
      * @param lon2 longitude do segundo codigo postal
      * @return distancia entre codigos postais
      */
    public static double distancia(double lat1, double lon1, double lat2, double lon2) {
        final double R = 6371e3;
        double theta1 = Math.toRadians(lat1);
        double theta2 = Math.toRadians(lat2);
        double deltaTheta = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);
        double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2) + Math.cos(theta1) * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; // distância em metros
        return d;

    }

    public AreaGeografica getAreaGeograficaByDesig(String desig) {
        for (AreaGeografica areageo : this.m_lstAreasGeograficas) {
            if (areageo.hasDesig(desig)) {
                return areageo;
            }
        }
        return null;
    }
    
}
