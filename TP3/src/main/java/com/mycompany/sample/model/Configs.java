package com.mycompany.sample.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author ZDPessoa
 */
public class Configs {
    
    public static Properties getConfigVals() throws IOException {
        
        File fichConfiguracao = new File("config.properties");
        FileReader reader = new FileReader(fichConfiguracao);
        if(reader == null){
            throw new FileNotFoundException("Config Not Found");
        }
        Properties props = new Properties();
        props.load(reader);
        
        return props;
        
    }
    /** Criar Empresa.
     * 
     * @param props propriedades da Empresa.
     * @return Empresa
     * @throws IOException 
     */
    public static Empresa criarEmpresa(Properties props) throws IOException{
        
        String desig = props.getProperty("Empresa.Designacao");
        String NIF = props.getProperty("Empresa.NIF");
        
        Empresa emp = new Empresa(desig, NIF);
        
        return emp;
    }
    
    public static List<TipoServico> criaTiposServicosSuportados(Properties props) throws IOException {
        
    List<TipoServico> listTipos = new ArrayList<TipoServico>();
    // Conhecer quantos TipoServico são suportados
    String qtdTipos = props.getProperty("Empresa.QuantidadeTiposServicoSuportados");
    int qtd = Integer.parseInt(qtdTipos);
    // Por cada tipo suportado criar a instância respetiva
    for (int i=1; i<=qtd; i++)
    {
    // Conhecer informação (descrição e classe) da instância a criar
        String desc = props.getProperty("Empresa.TipoServico."+ i +".Designacao");
        String classe = props.getProperty("Empresa.TipoServico." + i + ".Classe");
    // Criar a instância
        TipoServico tipoServ = new TipoServico(desc, classe);
    // Adicionar à lista a devolver
        listTipos.add(tipoServ);
    }   
    // Retornar Tipos Suportados
    return listTipos;
}
    
}
