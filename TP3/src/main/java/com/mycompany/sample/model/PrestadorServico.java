package com.mycompany.sample.model;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** Prestador de Servicos da Empresa.
 * 
 */

public class PrestadorServico {

    private int numero;
    private String nome;
    private String abrev;
    private String email;
    private List<Categoria> m_lstCategoria = new ArrayList<Categoria>();
    private List<AreaGeografica> m_lstAreasGeograficas = new ArrayList<AreaGeografica>();
    private ListaDisponibilidadesDiaria listaDispDiaria = new ListaDisponibilidadesDiaria();
    private List<PedidoPrestacaoServico>  listaPedidosPrestacao = new ArrayList<PedidoPrestacaoServico>();
    
    private static final int NUMERO_POR_OMISSAO = 0;
    private static final String NOME_POR_OMISSAO = "SEM NOME";
    private static final String ABREV_POR_OMISSAO = "SEM ABREVIATURA";
    private static final String EMAIL_POR_OMISSAO = "SEM EMAIL";

    /** Construtor de prestador de servicos
     * 
     * @param numero numero de empresa do prestador de servico
     * @param nome nome do prestador de servico
     * @param abrev nome abreviado do prestador de servico
     * @param email email do prestador de servico
     * @param areageo area geografica em que o prestador de servico atua
     * @param cat categoria onde prestador de servico realiza servicos
     */
    public PrestadorServico(int numero, String nome, String abrev, String email, AreaGeografica areageo, Categoria cat){

        if ( (numero < 0) || (nome == null) || (abrev == null) || (email == null) || (email.isEmpty())|| (nome.isEmpty()) || (abrev.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.abrev = abrev;
        this.email = email;
        this.nome = nome;
        this.numero = numero;
        m_lstAreasGeograficas.add(areageo);
        m_lstCategoria.add(cat);

    }
    
    /** Construtor de um prestador de servicos por omissao de dados
     * 
     */
    public PrestadorServico(){
        
        this.numero = NUMERO_POR_OMISSAO;
        this.nome = NOME_POR_OMISSAO;
        this.abrev = ABREV_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
    }
    
    public String getEmail(){
        
        return this.email;
    }
    
    public String getNome(){
        
        return this.nome;
    }
    
    public List<AreaGeografica> getListAreaGeografica(){
        
        List<AreaGeografica> newListAreaGeo = new ArrayList<AreaGeografica>();
        
        newListAreaGeo = this.m_lstAreasGeograficas;
        
        return newListAreaGeo;
        
    }
    
    public void setListaDisponibilidade(ListaDisponibilidadesDiaria listaDisponibilidadeDiaria){
        
        this.listaDispDiaria = listaDisponibilidadeDiaria;
    }
    
    public List<Disponibilidade> getListaDisponibilidade(){
        
        return this.listaDispDiaria.getListaDisp();
        
    }
    
    public ListaDisponibilidadesDiaria getListaDisponibilidadeDiaria(){
        return this.listaDispDiaria;
    }
    
    /** Adicionar uma area geografica ao prestador de servicos
     * 
     * @param areageo areageografica a ser adicionada
     * @return se foi ou nao adicionada
     */
    public boolean addAreaGeografica(AreaGeografica areageo)
    {
        return this.m_lstAreasGeograficas.add(areageo);
    }
    
    /** Adicionar um pedido de prestacao ao prestador de servico
     * 
     * @param ped pedido a ser adicionado
     * @return se foi ou nao adicionado
     */
    public boolean addPedidoPrestacao(PedidoPrestacaoServico ped){
        
        return this.listaPedidosPrestacao.add(ped);
    }

    /** Remover uma area geografica do prestador de servicos
     * 
     * @param areageo area geografica a ser removida
     * @return se foi ou nao removida
     */
    public boolean removeAreaGeografica(AreaGeografica areageo)
    {
        return this.m_lstAreasGeograficas.remove(areageo);
    }
    /** Adicionar categoria ao prestador
     * 
     * @param cat categoria a ser adicionada
     * @return se foi ou nao adicionada
     */
    public boolean addCategoria(Categoria cat)
    {
        return this.m_lstCategoria.add(cat);
    }

    /** Remover categoria do prestador
     * 
     * @param cat categoria a ser removida
     * @return se foi ou nao removida
     */
    public boolean removeCategoria(Categoria cat)
    {
        return this.m_lstCategoria.remove(cat);
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        PrestadorServico obj = (PrestadorServico) o;
        return (Objects.equals(numero, obj.numero));
    }

    @Override
    public String toString(){

        return String.format("%d - %s - %s - %s", this.numero, this.nome, this.abrev, this.email);

    }
    
    /** Verificar se prestador esta disponivel para pedido.
     * 
     * @param data1 data do pedido
     * @param hora hora do pedido
     * @param duracao duracao do pedido
     * @return se esta ou nao disponivel
     */
    public boolean isAvailable(Data data1, int hora, int duracao){
        
        int difHoras;
        
        for(Disponibilidade d : this.getListaDisponibilidade() ){
            if( d.getDataInicio().isMenor(data1) ){
                if(d.getDataFim().isMaior(data1)){
                    difHoras = d.getDataFim().difDataHora(data1, hora, d.getHoraF());
                    if( difHoras > (duracao/60) ){
                         return true;
                    }
                }
            }
        }
        return false;
    }
    
}
