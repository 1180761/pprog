/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public interface Servico {
    
    boolean hasId(String strId);
    double getCustoParaDuracao(int duracao);
    
    Categoria getCategoria();
    String getId();
    
    
}
