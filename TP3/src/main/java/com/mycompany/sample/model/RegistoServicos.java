package com.mycompany.sample.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** Registo de Servicos da Empresa.
 *
 */
public class RegistoServicos {

    private final Set<Servico> m_lstServicos = new HashSet<>();

    public List<Servico> getServicosDeCategoria(String idCat) {
        List<Servico> ls = new ArrayList<>();
        for (Servico serv : m_lstServicos) {
            if (serv.getCategoria().getCodigo().equals(idCat)) {
                ls.add(serv);
            }
        }
        return ls;
    }

    public Servico getServicoById(String strId) {
        for (Servico serv : this.m_lstServicos) {
            if (serv.hasId(strId)) {
                return serv;
            }
        }

        return null;
    }

    public boolean registaServico(Servico oServico) {
        if (this.validaServico(oServico)) {
            return addServico(oServico);
        }
        return false;
    }

    private boolean addServico(Servico oServico) {
        return m_lstServicos.add(oServico);
    }

    public boolean validaServico(Servico oServico) {
        boolean bRet = true;
        //valida
        return bRet;
    }

}
