/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.IndicarDisponiblidadeDiariaController;
import com.mycompany.sample.model.Data;
import com.mycompany.sample.model.Disponibilidade;
import java.util.Scanner;

/**
 *
 * @author ZDPessoa
 */
public class IndicarDisponiblidadeDiariaUI {
    
    

    public void indicaDisponibilidadeController() {

        IndicarDisponiblidadeDiariaController dispController = new IndicarDisponiblidadeDiariaController();

        indicaDisponibilidade(dispController);
    }

    public void indicaDisponibilidade(IndicarDisponiblidadeDiariaController controller) {

        Scanner scanner = new Scanner(System.in);
      
        controller.indicarNovasDisponiblidades();
        
        boolean verifica = true;
        do{
        System.out.printf("\nIntroduzir Data de Início:");
        System.out.printf("\nDia: ");
        int dia = scanner.nextInt();
        System.out.printf("\nMês: ");
        int mes = scanner.nextInt();
        System.out.printf("\nAno: ");
        int ano = scanner.nextInt();

        Data newdatai = new Data(dia, mes, ano);

        System.out.printf("\nIntroduzir Hora Início: ");
        int horai = scanner.nextInt();

        System.out.printf("\nIntroduzir Data de Fim:");
        System.out.printf("\nDia: ");
        dia = scanner.nextInt();
        System.out.printf("\nMês: ");
        mes = scanner.nextInt();
        System.out.printf("\nAno: ");
        ano = scanner.nextInt();

        Data newdataf = new Data(dia, mes, ano);

        System.out.printf("\nIntroduzir Hora de Fim: ");
        int horaf = scanner.nextInt();
        
        Disponibilidade newDisp = controller.novoPeriodoDisponiblidade(newdatai, horai, newdataf, horaf);

        System.out.println("\n" + newDisp);
        
        System.out.println("\nDeseja confirmar a Indicação de Disponibilidade?\n1. Sim\n2. Não");

        if (scanner.nextInt() == 1) {
            controller.registoPeriodoDisponibilidade();
            System.out.println("\nIndicação de Disponibilidade adicionada com sucesso...");
        } else {
            System.out.println("\nPedido De Indicação de Disponibilidade Cancelado");
        }
        
            System.out.println("\nDeseja efetuar outra Indicação de Disponibilidade?\n1. Sim\n2. Não");
            if (scanner.nextInt() == 2){
                verifica = false;
            }
        
        }while(verifica);
        
    }

}
