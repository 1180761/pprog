/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.AtribuirPedidoPrestacaoController;
import com.mycompany.sample.model.PedidoPrestacaoServico;
import com.mycompany.sample.model.PrestadorServico;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ZDPessoa
 */
public class AtribuirPedidoPrestacaoUI {

    private List<PrestadorServico> lps;
    private List<PedidoPrestacaoServico> lpps;
    private PedidoPrestacaoServico ped;
    private PrestadorServico prestadorServ;

    public void atribuirPedidoController() {

        AtribuirPedidoPrestacaoController controller = new AtribuirPedidoPrestacaoController();

        atribuirPedidoPrestacao(controller);
    }

    public void atribuirPedidoPrestacao(AtribuirPedidoPrestacaoController controller) {

        Scanner scanner = new Scanner(System.in);
        
        boolean verifica = true;
        do {
            
            lpps = controller.getListPedidoPrestacaoServico();

            if (lpps.isEmpty()) {
                System.out.println("\nNão existem Pedidos de Prestação de Serviço...");
                break;
            } else {

                ped = escolhePedidoPrestacaoServico(scanner);

                lps = controller.sortPrestadorPedido(ped);

                if (lps.isEmpty()) {
                    System.out.println("\nNão existem Prestadores de Serviço Disponiveis...");
                    break;
                } else {
                    prestadorServ = escolhePrestadorServico(scanner);

                    controller.atribuirPedidoPrestacao(prestadorServ, ped);

                    controller.removePedidoPrestacao(ped);

                    System.out.println("\nPedido de Prestação de Serviço atribuido com sucesso...");
                }

                System.out.println("\nDeseja Atribuir Mais algum Pedido?\n1. Sim\n2. Não");

                if (scanner.nextInt() == 1) {
                   verifica = true;
                }else{
                    break;
                }
            }
            
        }while(verifica);

    }

    public PrestadorServico escolhePrestadorServico(Scanner scanner) {
        boolean verify = false;
        int chr = 0;
        while (verify == false) {

            System.out.println("\n----Lista De Prestadores de Serviço----");
            displayList(lps);
            System.out.println("\nEscolha um Prestador: ");
            chr = scanner.nextInt();
            if (validaEscolha(chr, lps.size())) {
                verify = true;
            } else {
                System.out.println("\nEscolha inválida\n");
            }
        }
        return lps.get(chr - 1);

    }

    public PedidoPrestacaoServico escolhePedidoPrestacaoServico(Scanner scanner) {
        boolean verify = false;
        int chr = 0;
        while (verify == false) {
            System.out.println("\n----Lista De Pedidos de Prestação de Serviço----");
            displayList(lpps);
            System.out.println("\nEscolha uma Pedido: ");
            chr = scanner.nextInt();
            if (validaEscolha(chr, lpps.size())) {
                verify = true;
            } else {
                System.out.println("\nEscolha inválida\n");
            }
        }
        return lpps.get(chr - 1);

    }

    private static <E> void displayList(List<E> le) {

        int count = 1;
        for (E e : le) {
            System.out.println("\n" + count + ". ");
            System.out.println(e.toString());
            count++;
        }
    }

    public boolean validaEscolha(int chr, int size) {
        if (chr > 0 && chr <= (size + 1)) {
            return true;
        }
        return false;
    }

}
