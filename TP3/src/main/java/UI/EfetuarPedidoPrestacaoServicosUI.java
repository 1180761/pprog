/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Controller.EfetuarPedidoPrestacaoServicosController;
import com.mycompany.sample.model.Categoria;
import com.mycompany.sample.model.Data;
import com.mycompany.sample.model.EnderecoPostal;
import com.mycompany.sample.model.Servico;
import com.mycompany.sample.model.ServicoFixo;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author jorgi
 */
public class EfetuarPedidoPrestacaoServicosUI {

    private List<EnderecoPostal> lep;
    private EnderecoPostal endP;
    private List<Categoria> lc;
    private String idCat;
    private List<Servico> ls;
    private Servico serv;
    private String idServ;
    private String descricaoServ;
    private int duracao;

    public void Pedido() throws IOException {
        EfetuarPedidoPrestacaoServicosController ctrl = new EfetuarPedidoPrestacaoServicosController();
        novoPedidoPrest(ctrl);
    }

    public void novoPedidoPrest(EfetuarPedidoPrestacaoServicosController ctrl) throws IOException {
        Scanner scanner = new Scanner(System.in);
        this.lep = ctrl.novoPedido();
        endP = escolheEnderecoPostal(scanner);
        ctrl.setEnderecoPostal(endP.getM_strLocal(), endP.getCodPostal(), endP.getM_strLocalidade());

        do {
            this.lc = ctrl.getCategorias();
            idCat = escolheCategoria(scanner);

            this.ls = ctrl.getServicosDeCategoria(idCat);
            idServ = escolheServico(scanner);

            scanner.nextLine();
            System.out.println("\nEfetue a descrição da tarefa requerida...");
            descricaoServ = scanner.nextLine();

            if (ctrl.checkIdServDuracao(idServ)) {
                serv = ctrl.getServicoById(idServ);
                ServicoFixo ser = (ServicoFixo) serv;
                duracao = ser.getDuracao();
            } else {
                int dur;
                boolean valida = false;
                do {
                    System.out.println("\nIndique uma duração para o Serviço pedido...");
                    dur = scanner.nextInt();
                    if ((dur < 30) || (dur % 30 != 0)) {
                        System.out.println("Duração Invalida.");
                        valida = true;
                    } else {
                        duracao = dur;
                        break;
                    }
                } while (valida);

            }
            
            ctrl.addPedidoServico(idServ, descricaoServ, duracao);

            System.out.println("\n1. Adicionar outro Servico ao Pedido\n2. Seguinte.");
            if (scanner.nextInt() == 2) {
                break;
            }
        } while (true);

        do {
            System.out.println("Indique um horário para a realização da tarefa...");

            System.out.printf("Introduzir Data:");
            System.out.printf("\nDia: ");
            int dia = scanner.nextInt();
            System.out.printf("\nMês: ");
            int mes = scanner.nextInt();
            System.out.printf("\nAno: ");
            int ano = scanner.nextInt();
            Data newdata = new Data(dia, mes, ano);

            System.out.printf("\nIntroduzir Hora: ");
            int hora = scanner.nextInt();
            
            ctrl.addHorario(newdata, hora);

            System.out.println("1. Adicionar outro horário ao Pedido.\n2. Seguinte.");

            if (scanner.nextInt() == 2) {
                break;
            }

        } while (true);
        ctrl.getCustoTotal();
        
        System.out.println("\n----Detalhes do Pedido----");
        System.out.println(ctrl.getPedidoServico().toString());

        System.out.println("Deseja confirmar o Pedido?\n1. Sim\n2. Não");

        if (scanner.nextInt() == 1) {
            ctrl.registaPedido();
            System.out.printf("\nPedido De Prestação de Servico criado com sucesso.\nO seu número de pedido: %d\n",
                    ctrl.getPedidoServico().getNumero());
        } else {
            System.out.println("Pedido De Prestação de Servico Cancelado");
        }

    }

    public String escolheCategoria(Scanner scanner) {
        boolean verify = false;
        int chr = 0;
        while (verify == false) {
            System.out.println("\n----Lista De Categorias----");
            displayList(lc);
            System.out.println("\nEscolha uma categoria: ");
            chr = scanner.nextInt();
            if (validaEscolha(chr, lc.size())) {
                verify = true;
            } else {
                System.out.println("\nEscolha inválida\n");
            }
        }
        return lc.get(chr - 1).getCodigo();

    }

    public String escolheServico(Scanner scanner) {

        boolean verify = true;
        int chr = 0;
        while (verify) {
            System.out.println("\n----Lista De Serviços----");
            displayList(ls);
            System.out.println("\nEscolha um Sevico: ");
            chr = scanner.nextInt();
            if (validaEscolha(chr, ls.size())) {
                verify = false;
            } else {
                System.out.println("\nEscolha inválida\n");
            }
        }

        return ls.get(chr - 1).getId();

    }

    public EnderecoPostal escolheEnderecoPostal(Scanner scanner) {
        boolean verify = false;
        int chr = 0;
        while (verify == false) {
            System.out.println("\n----Lista De Endereços Postais----");
            displayList(lep);
            System.out.println("\nEscolha um endereco postal: ");
            chr = scanner.nextInt();
            if (validaEscolha(chr, lep.size())) {
                verify = true;
            } else {
                System.out.println("\nEscolha inválida\n");
            }
        }
        EnderecoPostal[] array = lep.toArray(new EnderecoPostal[lep.size()]);
        return array[chr - 1];
    }

    private static <E> void displayList(List<E> le) {

        int count = 1;
        for (E e : le) {
            System.out.println(count + ". " + e.toString());
            count++;
        }
    }

    public boolean validaEscolha(int chr, int size) {
        if (chr > 0 && chr <= (size + 1)) {
            return true;
        }
        return false;
    }

}
