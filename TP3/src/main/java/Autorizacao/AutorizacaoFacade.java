package Autorizacao;

import Autorizacao.model.PapelUtilizador;
import Autorizacao.model.RegistoPapeisUtilizador;
import Autorizacao.model.RegistoUtilizadores;
import Autorizacao.model.SessaoUtilizador;
import Autorizacao.model.Utilizador;
import com.mycompany.sample.model.Constantes;


public class AutorizacaoFacade {
    private SessaoUtilizador m_oSessao = null;

    private final RegistoPapeisUtilizador m_oPapeis = new RegistoPapeisUtilizador();
    private final RegistoUtilizadores m_oUtilizadores = new RegistoUtilizadores();

    public boolean registaPapelUtilizador(String strPapel)
    {
        PapelUtilizador papel = this.m_oPapeis.novoPapelUtilizador(strPapel);
        return this.m_oPapeis.addPapel(papel);
    }

    public boolean registaPapelUtilizador(String strPapel, String strDescricao)
    {
        PapelUtilizador papel = this.m_oPapeis.novoPapelUtilizador(strPapel,strDescricao);
        return this.m_oPapeis.addPapel(papel);
    }

    public boolean registaUtilizador(String strNome, String strEmail, String strPassword)
    {
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        return this.m_oUtilizadores.addUtilizador(utlz);
    }

    public boolean registaUtilizadorComPapel(String strNome, String strEmail, String strPassword, String strPapel)
    {
        PapelUtilizador papel = this.m_oPapeis.procuraPapel(strPapel);
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        utlz.addPapel(papel);
        return this.m_oUtilizadores.addUtilizador(utlz);
    }

    public boolean registaUtilizadorComPapeis(String strNome, String strEmail, String strPassword, String[] papeis)
    {
        Utilizador utlz = this.m_oUtilizadores.novoUtilizador(strNome,strEmail,strPassword);
        for (String strPapel: papeis)
        {
            PapelUtilizador papel = this.m_oPapeis.procuraPapel(strPapel);
            utlz.addPapel(papel);
        }

        return this.m_oUtilizadores.addUtilizador(utlz);
    }

    public boolean existeUtilizador(String strId)
    {
        return this.m_oUtilizadores.hasUtilizador(strId);
    }


    public SessaoUtilizador doLogin(String strId, String strPwd)
    {
        Utilizador utlz = this.m_oUtilizadores.procuraUtilizador(strId);
        
        if (utlz != null)
        {
            if (utlz.hasPassword(strPwd)){
                this.m_oSessao = new SessaoUtilizador(utlz);
            }
        }
        
        return getSessaoAtual();
    }

    public SessaoUtilizador getSessaoAtual()
    {
        return this.m_oSessao;
    }

    public void doLogout()
    {
        if (this.m_oSessao != null)
            this.m_oSessao.doLogout();
        this.m_oSessao = null;
    }
    
        public void bootstrap()
    {
        registaPapelUtilizador(Constantes.PAPEL_ADMINISTRATIVO);
        registaPapelUtilizador(Constantes.PAPEL_CLIENTE);
        registaPapelUtilizador(Constantes.PAPEL_FRH);
        registaPapelUtilizador(Constantes.PAPEL_PRESTADOR_SERVICO);

        registaUtilizadorComPapel("Administrativo 1", "adm1", "1",Constantes.PAPEL_ADMINISTRATIVO);
        registaUtilizadorComPapel("Administrativo 2", "adm2@esoft.pt", "123456",Constantes.PAPEL_ADMINISTRATIVO);

        registaUtilizadorComPapel("FRH 1", "frh1@esoft.pt", "123456",Constantes.PAPEL_FRH);
        registaUtilizadorComPapel("FRH 2", "frh2@esoft.pt", "123456",Constantes.PAPEL_FRH);

        registaUtilizadorComPapeis("Martim", "martim@esoft.pt", "123456",new String[] {Constantes.PAPEL_FRH, Constantes.PAPEL_ADMINISTRATIVO});
    }
}