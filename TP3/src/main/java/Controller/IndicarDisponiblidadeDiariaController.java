/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Autorizacao.model.SessaoUtilizador;
import com.mycompany.sample.model.Data;
import com.mycompany.sample.model.Disponibilidade;
import com.mycompany.sample.model.ListaDisponibilidadesDiaria;
import com.mycompany.sample.model.PrestadorServico;
import com.mycompany.sample.model.RegistoPrestadorServicos;
import static com.mycompany.tp3.main.aut;
import static com.mycompany.tp3.main.emp;

/**
 *
 * @author jorgi
 */
public class IndicarDisponiblidadeDiariaController {

    private String email;
    private PrestadorServico pres;
    private RegistoPrestadorServicos rps;
    private SessaoUtilizador sessao;
    private ListaDisponibilidadesDiaria ldd;
    private Disponibilidade dispd;

    public void indicarNovasDisponiblidades() {
        sessao = aut.getSessaoAtual();
        email = sessao.getEmailUtilizador();
        rps = emp.getRegistoPrestadorServicos();
        pres = rps.getPrestadorServicos(email);
    }
    
    public Disponibilidade novoPeriodoDisponiblidade(Data dataI, int horarioI, Data dataF, int horarioF){
        ldd = pres.getListaDisponibilidadeDiaria();
        dispd = ldd.novoPedidoDisponibilidade(dataI, horarioI, dataF, horarioF);
        
        return dispd;
    }
    
    public void registoPeriodoDisponibilidade(){
        ldd.registaPeriodoDisponibilidade(dispd);
    }
    
}
