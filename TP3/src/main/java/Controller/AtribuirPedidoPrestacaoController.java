/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.mycompany.sample.model.PedidoPrestacaoServico;
import com.mycompany.sample.model.PrestadorServico;
import static com.mycompany.tp3.main.emp;
import java.util.List;


/**
 *
 * @author ZDPessoa
 */
public class AtribuirPedidoPrestacaoController {
    
    
    public void atribuirPedidoPrestacao(PrestadorServico prestadorServ, PedidoPrestacaoServico ped){
        
        prestadorServ.addPedidoPrestacao(ped);
        
    }
    
    public List<PedidoPrestacaoServico> getListPedidoPrestacaoServico(){
        
        return emp.getRegistoPedidosPrestacaoServicos().getListPedidoPrestacaoServico();
        
    }
    
    public List<PrestadorServico> sortPrestadorPedido(PedidoPrestacaoServico ped){
        
        return emp.getRegistoPrestadorServicos().sortPrestadorPedido(ped);
        
    }
    
    public boolean removePedidoPrestacao(PedidoPrestacaoServico ped){
        
        return emp.getRegistoPedidosPrestacaoServicos().removePedidoPrestacao(ped);
    }
    
    
    
    
    
}
