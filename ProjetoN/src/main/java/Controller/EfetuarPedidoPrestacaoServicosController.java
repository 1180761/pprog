/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Autorizacao.model.SessaoUtilizador;
import static Controller.AplicacaoGPSD.getInstance;
import static Main.Main.emp;
import com.mycompany.sample.model.Categoria;
import com.mycompany.sample.model.Cliente;
import com.mycompany.sample.model.CodigoPostal;
import com.mycompany.sample.model.Data;
import com.mycompany.sample.model.EnderecoPostal;
import com.mycompany.sample.model.PedidoPrestacaoServico;
import com.mycompany.sample.model.RegistoCategorias;
import com.mycompany.sample.model.RegistoClientes;
import com.mycompany.sample.model.RegistoPedidosPrestacaoServicos;
import com.mycompany.sample.model.RegistoServicos;
import com.mycompany.sample.model.Servico;
import java.util.List;

/**
 *
 * @author jorgi
 */
public class EfetuarPedidoPrestacaoServicosController {

    private Cliente cli;
    private EnderecoPostal endP;
    private AplicacaoGPSD app;
    private String email;
    private RegistoClientes rc;
    private SessaoUtilizador sessao;
    private List<EnderecoPostal> lep;
    private List<Categoria> lc;
    private List<Servico> ls;
    private RegistoPedidosPrestacaoServicos rp;
    private PedidoPrestacaoServico ped;
    private RegistoCategorias rcat;
    private RegistoServicos rs;
    private Servico s;

    public List<EnderecoPostal> novoPedido() {
        app = getInstance();
        sessao = app.getSessaoAtual();
        email = sessao.getEmailUtilizador();
        rc = emp.getRegistoClientes();
        cli = rc.getClienteByEmail(email);
        lep = cli.getEnderecosPostais();
        return lep;
    }

    public void setEnderecoPostal(String end, CodigoPostal codPostal, String local) {
        endP = cli.getEnderecoPostal(end, codPostal, local);
        rp = emp.getRegistoPedidosPrestacaoServicos();
        ped = rp.novoPedido(cli, endP);
    }

    public List<Categoria> getCategorias() {
        rcat = emp.getRegistoCategorias();
        lc = rcat.getCategorias();
        return lc;
    }

    public List<Servico> getServicosDeCategoria(String idCat) {
        rs = emp.getRegistoServicos();
        ls = rs.getServicosDeCategoria(idCat);
        return ls;
    }

    public void addPedidoServico(String idServ, String desc, int dur) {
        s = rs.getServicoById(idServ);
        ped.addPedidoServico(s, desc, dur);
    }

    public void addHorario(Data data, int hora) {
        ped.addHorario(data, hora);
    }
    
    public PedidoPrestacaoServico getPedidoServico(){
        
        return this.ped;
    }
    
    public double getCustoTotal(){
        double c = ped.getCustoTotal();
        return c;
    }
    
    public int registaPedido(){
        int num = rp.registaPedido(ped);
        return num;
    }
    
    
}
