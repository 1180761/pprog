/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jorgi
 */
public class RegistoCategorias {

    private final Set<Categoria> m_lstCategorias = new HashSet<>();

    public Categoria getCategoriaById(String strId) {
        for (Categoria cat : this.m_lstCategorias) {
            if (cat.hasId(strId)) {
                return cat;
            }
        }

        return null;
    }

    public Categoria novaCategoria(String strCodigo, String strDescricao) {
        return new Categoria(strCodigo, strDescricao);
    }

    public boolean registaCategoria(Categoria oCategoria) {
        if (this.validaCategoria(oCategoria)) {
            return addCategoria(oCategoria);
        }
        return false;
    }

    public boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }

    public boolean validaCategoria(Categoria oCategoria) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public List<Categoria> getCategorias() {
        List<Categoria> lc = new ArrayList<>();
        lc.addAll(this.m_lstCategorias);
        return lc;
    }

}
