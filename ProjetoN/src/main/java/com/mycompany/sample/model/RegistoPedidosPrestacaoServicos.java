/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ZDPessoa
 */
public class RegistoPedidosPrestacaoServicos {

    private final Set<PedidoPrestacaoServico> m_lstPedidosPrestacaoServico = new HashSet<>();

    /* public RegistoPedidosPrestacaoServicos(){
        
        this.pedidosPrestacaoServicoList = new ArrayList<PedidoPrestacaoServico>();
        
    } */
    public PedidoPrestacaoServico novoPedido(Cliente cli, EnderecoPostal endPostal) {

        PedidoPrestacaoServico novoPedPrestacaoServ = new PedidoPrestacaoServico(cli, endPostal);

        return novoPedPrestacaoServ;

    }

    public int registaPedido(PedidoPrestacaoServico pedPrestacaoServ) {

        pedPrestacaoServ.validaPedido();

        int numero = this.geraNumeroPedido();

        pedPrestacaoServ.setNumero(numero);

        this.addPedido(pedPrestacaoServ);

        this.notificaCliente(pedPrestacaoServ);

        return numero;

    }

    private void addPedido(PedidoPrestacaoServico novoPedPrestacaoServ) {

        this.m_lstPedidosPrestacaoServico.add(novoPedPrestacaoServ);

    }

    private int geraNumeroPedido() {

        int numero = 0;

        for (PedidoPrestacaoServico p : this.m_lstPedidosPrestacaoServico) {
            numero = p.getNumero();
        }

        return numero + 1;

    }

    private void notificaCliente(PedidoPrestacaoServico pedPrestacaoServ) {

        pedPrestacaoServ.getCliente().getEmail();

        // SEND EMAIL
    }
}
