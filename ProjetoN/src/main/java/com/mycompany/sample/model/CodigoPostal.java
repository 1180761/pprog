/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author jorgi
 */
public class CodigoPostal {
    
    private final String codPostal;
    private final float latitude;
    private final float longitude;
    
    private static final String COD_POSTAL_POR_OMISSAO = "SEM COD_POSTAL";
    private static final float LAT_POR_OMISSAO = 0;
    private static final float LOG_POR_OMISSAO = 0;
    
    public CodigoPostal(String codPostal, float latitude, float longitude){
        
        this.codPostal = codPostal;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public CodigoPostal(){
        
        this.codPostal = COD_POSTAL_POR_OMISSAO;
        this.latitude = LAT_POR_OMISSAO;
        this.longitude = LOG_POR_OMISSAO;
        
    }
    
    public float getLat(){
        
        return this.latitude;
    }
    
    public float getLog(){
        
        return this.longitude;
    }
    
}
