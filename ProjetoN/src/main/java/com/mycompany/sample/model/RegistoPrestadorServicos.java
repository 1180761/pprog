/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ZDPessoa
 */
public class RegistoPrestadorServicos {

    private final Set<PrestadorServico> m_lstPrestadoresServicos = new HashSet<>();

    public PrestadorServico getPrestadorServicos(String email) {

        for (PrestadorServico ps : this.m_lstPrestadoresServicos) {
            if (ps.getEmail().compareTo(email) == 0) {
                return ps;
            }
        }

        return null;
    }

    public boolean registaPrestadorServico(PrestadorServico prest) {
        if (this.validaPrestadorServico(prest)) {
            return addPrestadorServico(prest);
        }
        return false;
    }

    public boolean addPrestadorServico(PrestadorServico prest) {
        return m_lstPrestadoresServicos.add(prest);
    }

    public boolean validaPrestadorServico(PrestadorServico prest) {
        boolean bRet = true;
        //valida
        return bRet;
    }

    public List<PrestadorServico> getPrestadorServicos() {
        List<PrestadorServico> lc = new ArrayList<>();
        lc.addAll(this.m_lstPrestadoresServicos);
        return lc;
    }

}
