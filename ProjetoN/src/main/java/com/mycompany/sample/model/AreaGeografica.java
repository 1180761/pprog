package com.mycompany.sample.model;

public class AreaGeografica {

    private String desig;
    private double custo;
    private double raio;
    private CodigoPostal codPostal;
    
    private static final String DESIG_POR_OMISSAO = "SEM DESIG";
    private static final double CUSTO_POR_OMISSAO = 0;
    private static final double RAIO_POR_OMISSAO = 0;
    private static final CodigoPostal COD_POSTAL_POR_OMISSAO = new CodigoPostal();

    public AreaGeografica(String desig, double custo, double raio, CodigoPostal codPostal) {
// valida se os dados necessários para criar uma área geográfica são todos válidos
        if ((desig == null) || (desig.isEmpty()) || (custo <= 0) || (codPostal == null) || (raio <= 0)) {
            throw new IllegalArgumentException("Argumentos inválidos.");
        } else {
            // guarda a informação da área geográfica que está a ser criada
            this.desig = desig;
            this.custo = custo;
            this.raio = raio;
            this.codPostal = codPostal;
        }
    }
    
    public AreaGeografica(){
        
        this.codPostal = COD_POSTAL_POR_OMISSAO;
        this.custo = CUSTO_POR_OMISSAO;
        this.desig = DESIG_POR_OMISSAO;
        this.raio = RAIO_POR_OMISSAO;
     
    }

    public String getDesig() {
        return desig;
    }

    public double getCusto() {
        return custo;
    }

    public CodigoPostal getCodPostal() {
        return codPostal;
    }

    public double getRaio() {
        return raio;
    }
    
}
