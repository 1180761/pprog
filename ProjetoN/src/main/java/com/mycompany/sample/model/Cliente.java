
package com.mycompany.sample.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

public class Cliente {

    private String m_strNome;
    private String m_strNIF;
    private String m_strTelefone;
    private String m_strEmail;
    private List<EnderecoPostal> m_lstMoradas = new ArrayList<EnderecoPostal>();
    
    private static final String NOME_POR_OMISSAO = "SEM NOME";
    private static final String NIF_POR_OMISSAO = "SEM NIF";
    private static final String TELE_POR_OMISSAO = "SEM TELEFONE";
    private static final String EMAIL_POR_OMISSAO = "SEM EMAIL";

    public Cliente(String strNome, String strNIF, String strTelefone, String strEmail, EnderecoPostal oMorada)
    {
        if ( (strNome == null) || (strNIF == null) || (strTelefone == null) ||
                (strEmail == null) || (oMorada == null) ||
                (strNome.isEmpty())|| (strNIF.isEmpty()) || (strTelefone.isEmpty()) ||
                (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.m_strNome = strNome;
        this.m_strEmail = strEmail;
        this.m_strNIF = strNIF;
        this.m_strTelefone = strTelefone;
        m_lstMoradas.add(oMorada);
    }
    
    public Cliente(){
        
        this.m_strNome = NOME_POR_OMISSAO;
        this.m_strNIF = NIF_POR_OMISSAO;
        this.m_strTelefone = TELE_POR_OMISSAO;
        this.m_strEmail = EMAIL_POR_OMISSAO;
        
    }

    public String getNome()
    {
        return this.m_strNome;
    }

    public String getEmail()
    {
        return this.m_strEmail;
    }
    
    public EnderecoPostal getEnderecoPostal(String end, CodigoPostal codPostal, String local){
        for(EnderecoPostal endP : m_lstMoradas){
            if(end.equals(endP.getM_strLocal()) && codPostal.equals(endP.getCodPostal()) && local.equals(endP.getM_strLocalidade())){
                return endP;
            }
        }
        return null;
    }

    public List<EnderecoPostal> getEnderecosPostais() {
        List<EnderecoPostal> lep = new ArrayList<>();
        lep.addAll(this.m_lstMoradas);
        return lep;
    }

    public boolean hasEmail(String strEmail)
    {
        return this.m_strEmail.equalsIgnoreCase(strEmail);
    }

    public boolean addEnderecoPostal(EnderecoPostal oMorada)
    {
        return this.m_lstMoradas.add(oMorada);
    }

    public boolean removeEnderecoPostal(EnderecoPostal oMorada)
    {
        return this.m_lstMoradas.remove(oMorada);
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strEmail);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Cliente obj = (Cliente) o;
        return (Objects.equals(m_strEmail, obj.m_strEmail) || Objects.equals(m_strNIF, obj.m_strNIF));
    }

    @Override
    public String toString()
    {
        String str = String.format("%s - %s - %s - %s", this.m_strNome, this.m_strNIF, this.m_strTelefone, this.m_strEmail);
        for(EnderecoPostal morada:this.m_lstMoradas)
            str += "\nMorada:\n" + morada.toString();
        return str;
    }

    public static EnderecoPostal novoEnderecoPostal(String strLocal, CodigoPostal strCodPostal, String strLocalidade)
    {
        return new EnderecoPostal(strLocal,strCodPostal,strLocalidade);
    }

}
