/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import Autorizacao.AutorizacaoFacade;


public class Empresa {

    private String m_strDesignacao;
    private String m_strNIF;
    private final AutorizacaoFacade m_oAutorizacao;
    private final RegistoAreasGeograficas m_oRegistoAreasGeogras;
    private final RegistoCategorias m_oRegistoCategorias;
    private final RegistoClientes m_oRegistoClientes;
    private final RegistoPrestadorServicos m_oRegistoPrestadorServicos;
    private final RegistoServicos m_oRegistoServicos;
    private final RegistoPedidosPrestacaoServicos m_oRegistoPedidosPrestacaoServicos;

    public Empresa(String strDesignacao, String strNIF) {
        if ((strDesignacao == null) || (strNIF == null)
                || (strDesignacao.isEmpty()) || (strNIF.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;
        this.m_strNIF = strNIF;
        this.m_oRegistoAreasGeogras = new RegistoAreasGeograficas();
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_oRegistoCategorias = new RegistoCategorias();
        this.m_oRegistoClientes = new RegistoClientes();
        this.m_oRegistoPrestadorServicos = new RegistoPrestadorServicos();
        this.m_oRegistoServicos = new RegistoServicos();
        this.m_oRegistoPedidosPrestacaoServicos = new RegistoPedidosPrestacaoServicos();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    public RegistoAreasGeograficas getRegistoAreasGeograficas() {
        return this.m_oRegistoAreasGeogras;
    }
    
    public RegistoClientes getRegistoClientes(){
        return this.m_oRegistoClientes;
    }
    
    public RegistoCategorias getRegistoCategorias(){
        return this.m_oRegistoCategorias;
    }
    
    public RegistoPrestadorServicos getRegistoPrestadorServicos(){
        return this.m_oRegistoPrestadorServicos;
    }
    
    public RegistoServicos getRegistoServicos(){
        return this.m_oRegistoServicos;
    }
    
    public RegistoPedidosPrestacaoServicos getRegistoPedidosPrestacaoServicos(){
        return this.m_oRegistoPedidosPrestacaoServicos;
    }
}
