/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jorgi
 */
public class RegistoAreasGeograficas {

    private final Set<AreaGeografica> m_lstAreasGeograficas = new HashSet<>();

    public boolean registaAreaGeografica(AreaGeografica areageo) {
        if (this.validaAreaGeografica(areageo)) {
            return addAreaGeografica(areageo);
        }
        return false;
    }

    public boolean addAreaGeografica(AreaGeografica areageo) {
        return m_lstAreasGeograficas.add(areageo);
    }

    public boolean validaAreaGeografica(AreaGeografica areageo) {
        // valida
        return true;
    }
    
     public  AreaGeografica getAreaGeograficaMaisPerto(CodigoPostal codPostal){
        
        AreaGeografica areaGeo = new AreaGeografica();
        
        double dist = 0;
        double distAux = 0;
        
        for (AreaGeografica a : m_lstAreasGeograficas){
            
            dist = distancia(a.getCodPostal().getLat(), a.getCodPostal().getLog(), 
                    codPostal.getLat(), codPostal.getLog());
              
            if(dist < distAux ){
                
               areaGeo = a;
                
               distAux = dist;
            }
            
        }
        
        return areaGeo;
    }

    public double distancia(double lat1, double lon1, double lat2, double lon2) {
        final double R = 6371e3;
        double theta1 = Math.toRadians(lat1);
        double theta2 = Math.toRadians(lat2);
        double deltaTheta = Math.toRadians(lat2 - lat1);
        double deltaLambda = Math.toRadians(lon2 - lon1);
        double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2) + Math.cos(theta1) * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; // distância em metros
        return d;

    }
}
