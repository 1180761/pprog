/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EnderecoPostal {

    private String m_strLocal;
    private String m_strLocalidade;
    private CodigoPostal codPostal;

    private static final String LOCAL_POR_OMISSAO = "SEM LOCAL";

    private static final String COD_POSTAL_POR_OMISSAO = "SEM COD_POSTAL";

    private static final String LOCALIDADE_POR_OMISSAO = "SEM LOCALIDADE";

    public EnderecoPostal(String strLocal, CodigoPostal codPostal, String strLocalidade) {
        if ((strLocal == null) || (strLocalidade == null)
                || (strLocal.isEmpty()) || (strLocalidade.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strLocal = strLocal;
        this.m_strLocalidade = strLocalidade;
        this.codPostal = codPostal;
    }

    public EnderecoPostal() {

        this.m_strLocal = LOCAL_POR_OMISSAO;
        this.m_strLocalidade = LOCALIDADE_POR_OMISSAO;

    }

    public CodigoPostal getCodPostal() {
        return codPostal;
    }

    public String getM_strLocal() {
        return m_strLocal;
    }

    public String getM_strLocalidade() {
        return m_strLocalidade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hash(this.m_strLocal, this.m_strLocalidade);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o) {
            return true;
        }
        // null check
        if (o == null) {
            return false;
        }
        // type check and cast
        if (getClass() != o.getClass()) {
            return false;
        }
        // field comparison
        EnderecoPostal obj = (EnderecoPostal) o;
        return (Objects.equals(m_strLocal, obj.m_strLocal)
                && Objects.equals(m_strLocalidade, obj.m_strLocalidade));
    }

    @Override
    public String toString() {
        return String.format("%s \n %s - %s", this.m_strLocal, this.m_strLocalidade, this.codPostal);
    }

}
