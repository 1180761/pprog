/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public class DescricaoServicoPedido {
    
    private String descricao;
    
    private int duracao;
    
    private Servico serv;
    
    public DescricaoServicoPedido(Servico serv, String descricao, int duracao){
        
        this.descricao = descricao;
        this.duracao = duracao;
        this.serv = serv;
    }
    
    public double getCusto(){
        
        return this.serv.getCustoParaDuracao(duracao);
    }
    
    
}
