/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import static Main.Main.emp;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author ZDPessoa
 */
public class PedidoPrestacaoServico {

    private int numero;

    private Data Data;

    private double total;

    private Cliente cliente;

    private AreaGeografica areageo;

    private EnderecoPostal endPostal;

    private OutroCusto oCusto;

    private List<DescricaoServicoPedido> DescServList = new ArrayList<DescricaoServicoPedido>();

    private List<PreferenciaHorario> PrefHorList = new ArrayList<PreferenciaHorario>();

    private List<OutroCusto> OutroCustoList = new ArrayList<OutroCusto>();

    private static final int NUMERO_POR_OMISSAO = 0;

    private static final Data DATA_POR_OMISSAO = new Data();

    private static final float TOTAL_POR_OMISSAO = 0;

    private static final Cliente CLIENTE_POR_OMISSAO = new Cliente();

    private static final EnderecoPostal END_P_POR_OMISSAO = new EnderecoPostal();

    public PedidoPrestacaoServico(Cliente cliente, EnderecoPostal endPostal) {

        if ((cliente == null) || (endPostal == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = cliente;
        this.endPostal = endPostal;

    }

    public PedidoPrestacaoServico() {

        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = CLIENTE_POR_OMISSAO;
        this.endPostal = END_P_POR_OMISSAO;

    }

    public Cliente getCliente() {

        return this.cliente;
    }

    public int getNumero() {

        return this.numero;
    }

    public void setNumero(int numero) {

        this.numero = numero;

    }

    public DescricaoServicoPedido addPedidoServico(Servico serv, String desc, int dur) {

        DescricaoServicoPedido novoPedido = new DescricaoServicoPedido(serv, desc, dur);

        return novoPedido;

    }

    public void addPedidoServico(DescricaoServicoPedido novoPedido) {

        this.DescServList.add(novoPedido);

    }

    public void addHorario(Data Data, int hora) {

        int ordem = countHorarios();

        PreferenciaHorario newHorario = new PreferenciaHorario(ordem, Data, hora);

        this.PrefHorList.add(newHorario);

    }

    private int countHorarios() {

        int ordem = 0;

        for (PreferenciaHorario h : this.PrefHorList) {
            ordem = h.getOrdem();
        }

        return ordem + 1;

    }

    public DescricaoServicoPedido getDSP(int i) {

        for (int j = 0; j < this.DescServList.size(); j++) {

            if (j == i) {
                return this.DescServList.get(j);
            }

        }

        return null;

    }

    public double getCustoTotal() {
        double c = calculaCusto();
        return c;
    }

    public double calculaCusto() {

        double custo = 0;

        for (DescricaoServicoPedido dp : this.DescServList) {
            custo += dp.getCusto();
        }

        this.OutroCustoList.clear();

        areageo = emp.getRegistoAreasGeograficas().getAreaGeograficaMaisPerto(this.endPostal.getCodPostal());

        double custoDeslocacao = areageo.getCusto();

        oCusto = new OutroCusto(areageo.getDesig(), custoDeslocacao);

        this.OutroCustoList.add(oCusto);

        custo += custoDeslocacao;

        this.total = custo;

        return custo;
    }

    public boolean validaPedido() {

        if ((this.cliente == null) || (this.endPostal == null) || (this.Data == null) || (this.numero <= 0) || (this.total <= 0)
                || (this.DescServList == null) || (this.OutroCustoList == null) || (this.PrefHorList == null)
                || (this.Data == DATA_POR_OMISSAO) || (this.endPostal == END_P_POR_OMISSAO) || (this.cliente == CLIENTE_POR_OMISSAO)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio...");
        }

        return true;
    }
}
