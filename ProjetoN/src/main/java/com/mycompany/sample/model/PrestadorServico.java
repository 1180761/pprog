/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PrestadorServico {

    private int numero;
    private String nome;
    private String abrev;
    private String email;
    private List<Categoria> m_lstCategoria = new ArrayList<Categoria>();
    private List<AreaGeografica> m_lstAreasGeograficas = new ArrayList<AreaGeografica>();
    private ListaDisponibilidadesDiaria listaDispDiaria = new ListaDisponibilidadesDiaria();
    
    private static final int NUMERO_POR_OMISSAO = 0;
    private static final String NOME_POR_OMISSAO = "SEM NOME";
    private static final String ABREV_POR_OMISSAO = "SEM ABREVIATURA";
    private static final String EMAIL_POR_OMISSAO = "SEM EMAIL";

    public PrestadorServico(int numero, String nome, String abrev, String email, AreaGeografica areageo, Categoria cat){

        if ( (numero < 0) || (nome == null) || (abrev == null) || (email == null) || (email.isEmpty())|| (nome.isEmpty()) || (abrev.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.abrev = abrev;
        this.email = email;
        this.nome = nome;
        this.numero = numero;
        m_lstAreasGeograficas.add(areageo);
        m_lstCategoria.add(cat);

    }
    
    public PrestadorServico(){
        
        this.numero = NUMERO_POR_OMISSAO;
        this.nome = NOME_POR_OMISSAO;
        this.abrev = ABREV_POR_OMISSAO;
        this.email = EMAIL_POR_OMISSAO;
    }
    
    public String getEmail(){
        
        return this.email;
    }
    
    public void setListaDisponibilidade(ListaDisponibilidadesDiaria listaDisponibilidadeDiaria){
        
        this.listaDispDiaria = listaDisponibilidadeDiaria;
    }
    
    public List<Disponibilidade> getListaDisponibilidade(){
        
        return this.listaDispDiaria.getListaDisp();
        
    } 
    
    public boolean addAreaGeografica(AreaGeografica areageo)
    {
        return this.m_lstAreasGeograficas.add(areageo);
    }

    public boolean removeAreaGeografica(AreaGeografica areageo)
    {
        return this.m_lstAreasGeograficas.remove(areageo);
    }

    public boolean addCategoria(Categoria cat)
    {
        return this.m_lstCategoria.add(cat);
    }

    public boolean removeCategoria(Categoria cat)
    {
        return this.m_lstCategoria.remove(cat);
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.numero);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/

        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        PrestadorServico obj = (PrestadorServico) o;
        return (Objects.equals(numero, obj.numero));
    }

    @Override
    public String toString(){

        return String.format("%d - %s - %s - %s", this.numero, this.nome, this.abrev, this.email);

    }
}
