/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Autorizacao.AutorizacaoFacade;
import UI.EfetuarPedidoPrestacaoServicosUI;
import com.mycompany.sample.model.Empresa;
import com.mycompany.sample.model.Guardar;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author jorgi
 */
public class Main {

    public static Empresa emp;

    public static void main(String[] args) throws IOException {
        Guardar guardar = new Guardar();
        emp = guardar.guardaFicheiros();
        showMenu();

    }

    public static void showMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Menu"
                + "1 - Efetuar Pedido de Prestação de Serviços"
                + "2 - Indicar disponibilidade	diária de prestação de serviços");
        int chr = scanner.nextInt();
        switch (chr) {
            case 1:
                System.out.println("Introduza o seu Id");
                String nome = scanner.nextLine();
                System.out.println("Introduza a sua password");
                String pass = scanner.nextLine();
                AutorizacaoFacade aut = emp.getAutorizacaoFacade();
                aut.doLogin(nome, pass);
                EfetuarPedidoPrestacaoServicosUI ui = new EfetuarPedidoPrestacaoServicosUI();
                ui.Pedido();
        }
    }

}
