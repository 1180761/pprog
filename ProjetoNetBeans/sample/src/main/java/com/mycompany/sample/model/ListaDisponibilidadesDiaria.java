/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author ZDPessoa
 */
public class ListaDisponibilidadesDiaria{
    
    private List<Disponibilidade> dispList;
    
    public ListaDisponibilidadesDiaria(){
        
        this.dispList = new ArrayList<Disponibilidade>();
        
    }
    
    public List<Disponibilidade> getListaDisp(){
     
        List<Disponibilidade> newDispList = new ArrayList<Disponibilidade>();
        
        newDispList = this.dispList;
        
        return newDispList;
    }
    
    public Disponibilidade novoPedidoDisponibilidade(Data dataInicio, int horaInicio, Data dataFim, int horaFim){
        
        Disponibilidade novaDisp = new Disponibilidade( dataInicio, horaInicio, dataFim, horaFim);
        
        return novaDisp;
        
    }
    
    public void addPeriodoDisponibilidade(Disponibilidade novaDisp){
        
        this.dispList.add(novaDisp);    
        
    }
    
    public void registaPeriodoDisponibilidade(Disponibilidade novaDisp){
        
        addPeriodoDisponibilidade(novaDisp);
       //Método Valida Disp
        
    }
    
    
    
}
