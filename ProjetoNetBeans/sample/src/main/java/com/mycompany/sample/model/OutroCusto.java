/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public class OutroCusto {
    
    private String designacao;
    private float valor;
    
    private static final String DESIG_POR_OMISSAO = "SEM DESIGNACAO";
    private static final float VALOR_POR_OMISSAO = 0;
    
    public OutroCusto(String designacao, float valor){
        
        this.designacao = designacao;
        this.valor = valor;
        
    }
    
    public OutroCusto(){
        
        this.designacao = DESIG_POR_OMISSAO;
        this.valor = VALOR_POR_OMISSAO;
        
    }
    
}
