/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public class PreferenciaHorario {
    
    private int ordem;
    
    private Data data;
    
    private int hora;
    
    public PreferenciaHorario (int ordem, Data data, int hora){
        
        this.ordem = ordem;
        this.data = data;
        this.hora = hora;
        
    }
    
    public int getOrdem(){
        
        return this.ordem;
    }
    
}
