package com.mycompany.sample.model;

public class AreaGeografica {

    private String desig;
    private double custo;
    private String codPostal;
    private double raio;

    public AreaGeografica(String desig, double custo, String codPostal, double raio) {
// valida se os dados necessários para criar uma área geográfica são todos válidos
        if ((desig == null) || (desig.isEmpty()) || (custo <= 0) || (codPostal == null) || (codPostal.isEmpty()) || (raio <= 0)) {
            throw new IllegalArgumentException("Argumentos inválidos.");
        } else {
            // guarda a informação da área geográfica que está a ser criada
            this.desig = desig;
            this.custo = custo;
            this.codPostal = codPostal;
            this.raio = raio;
        }
    }

    public String getDesig() {
        return desig;
    }

    public double getCusto() {
        return custo;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public double getRaio() {
        return raio;
    }
}
