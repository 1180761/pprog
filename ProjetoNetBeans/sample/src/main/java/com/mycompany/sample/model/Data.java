/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public class Data {
    
    /**
     * O ano da data.
     */
    private int ano;
    
    /**
    * O mês da data.
    */
    private int mes;
     
    /**
    * O dia da data.
    */
    private int dia;
        
    /**
    * O ano da data por omissão.
    */
    private static final int ANO_POR_OMISSAO = 1;
    
    /**
    * O mês da data por omissão.
    */
    private static final int MES_POR_OMISSAO = 1;
    
    /**
    * O dia da data por omissão.
    */
    private static final int DIA_POR_OMISSAO = 1;
    
    /**
    * Nomes dos meses do ano.
    */
    private static String[] nomeMes = {"Inválido", "Janeiro", "Fevereiro",
                                       "Março", "Abril", "Maio", "Junho",
                                       "Julho", "Agosto", "Setembro",
                                       "Outubro", "Novembro", "Dezembro"};
   
    /**
     * Constrói uma instância de Data recebendo o ano, o mês e o dia. 
     * 
     * @param ano o ano da data
     * @param mes o mês da data
     * @param dia o dia da data
     */
    public Data(int ano, int mes, int dia) {        
        this.ano = ano;
        this.mes = mes;
        this.dia = dia;              
    }

    /**
     * Constrói uma instância de Data com a data por omissão.  
     */
    public Data() {
        ano = ANO_POR_OMISSAO;
        mes = MES_POR_OMISSAO;
        dia = DIA_POR_OMISSAO;
    }
    
    public String toString() {
        return String.format("%d/%s/%d", dia, nomeMes[mes], ano);
    }
    
    /**
     * Devolve o ano da data.
     * 
     * @return ano da data
     */
    public int getAno() {
        return ano;
    }
    
    /**
     * Devolve o mês da data.
     * 
     * @return mês da data
     */
    public int getMes() {
        return mes;
    }

    /**
     * Devolve o dia da data.
     * 
     * @return dia da data
     */    
    public int getDia() {
        return dia;
    }
    
    /**
     * Modifica o ano, o mês e o dia da data.
     * 
     * @param ano o novo ano da data
     * @param mes o novo mês da data
     * @param dia o novo dia da data
     */    
    public void setData(int ano, int mes, int dia) {
        this.ano = ano;         
        this.mes = mes;       
        this.dia = dia;          
    }
    
}
