/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author ZDPessoa
 */
public class RegistoPedidosPrestacaoServicos {
    
    private List<PedidoPrestacaoServico> pedidosPrestacaoServicoList;
    
    public RegistoPedidosPrestacaoServicos(){
        
        this.pedidosPrestacaoServicoList = new ArrayList<PedidoPrestacaoServico>();
        
    }
    
    public PedidoPrestacaoServico novoPedido(Cliente cli, EnderecoPostal endPostal){
        
        PedidoPrestacaoServico novoPedPrestacaoServ = new PedidoPrestacaoServico(cli, endPostal);
        
        return novoPedPrestacaoServ;
        
    }
    
    public int registaPedido(PedidoPrestacaoServico PedPrestacaoServ){
        
        int numero;
        
        //Valida Pedido
        
        numero = this.geraNumeroPedido();
        
        PedPrestacaoServ.setNumero(numero);
        
        this.addPedido(PedPrestacaoServ);
        
        //NotificaCliente
        
        return numero;
        
    }
    
    private void addPedido(PedidoPrestacaoServico novoPedPrestacaoServ){
        
        this.pedidosPrestacaoServicoList.add(novoPedPrestacaoServ);
        
    }
    
    private int geraNumeroPedido(){
        
        int numero = 0;
        
        for(PedidoPrestacaoServico p : this.pedidosPrestacaoServicoList )
            numero = p.getNumero();
        
        return numero + 1;
        
    }
    
    
    
}
