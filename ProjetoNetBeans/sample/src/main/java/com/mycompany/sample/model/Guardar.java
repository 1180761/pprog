/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Guardar {

    public void guardaFicheiros() throws IOException {
        Empresa emp = new Empresa("Empresa", "1");
        carregaFicheiroClientes(emp);
        //carregaFicheiroCategorias(emp);
        //carregaFicheiroServicos(emp);
        //carregaFicheiroAreasGeograficas(emp);

    }

    public String getPwd(String linha){
        String atributos[] = linha.split(";");
        return atributos[4];
    }

    public Cliente novoCliente(String linha) {
        String atributos[] = linha.split(";");
        EnderecoPostal end = new EnderecoPostal(atributos[5], atributos[6], atributos[7]);
        Cliente c = new Cliente(atributos[0], atributos[1], atributos[2], atributos[3], end);
        return c;
    }

    public void carregaFicheiroClientes(Empresa emp) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("clientes.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        int cont = 0;
        while ((linha = br.readLine()) != null) {
            Cliente c = novoCliente(linha);

                emp.registaCliente(c, getPwd(linha) );
        }
        br.close();
    }


    public Servico novoServico(String linha) {
        String atributos[] = linha.split(";");
        Categoria cat = new Categoria(atributos[4], atributos[5]);
        TipoServico tp = new TipoServico(...);
        Servico serv =  tp.novoServico(atributos[0], atributos[1], atributos[2], Double.valueOf(atributos[3]), cat);   
        return serv;
    }

    public void carregaFicheiroServicos(Empresa emp) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("servicos.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            Servico serv = novoServico(linha);
            emp.registaServico(serv);
        }
        br.close();
    }

    public Categoria novaCategoria(String linha) {
        String atributos[] = linha.split(";");
        Categoria cat = new Categoria(atributos[0], atributos[1]);
        return cat;
    }

    public void carregaFicheiroCategorias(Empresa emp) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("categorias.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            Categoria cat = novaCategoria(linha);
            emp.addCategoria(cat);
        }
        br.close();
    }

    public AreaGeografica novaAreaGeografica(String linha) {
        String atributos[] = linha.split(";");
        AreaGeografica areageo = new AreaGeografica(atributos[0], Double.valueOf(atributos[1]), atributos[2], Double.valueOf(atributos[3]));
        return areageo;
    }

    public void carregaFicheiroAreasGeograficas(Empresa emp) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("C:\\areasgeograficas.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            AreaGeografica areageo = novaAreaGeografica(linha);
            emp.addAreaGeografica(areageo);
        }
        br.close();
    }

    public PrestadorServico novoPrestadorServico(String linha) {
        String atributos[] = linha.split(";");
        AreaGeografica areageo = new AreaGeografica(atributos[4], Double.valueOf(atributos[5]), atributos[6], Double.valueOf(atributos[7]));
        Categoria cat = new Categoria(atributos[8], atributos[9]);
        PrestadorServico prest = new PrestadorServico(Integer.valueOf(atributos[0]), atributos[1], atributos[2], atributos[3], areageo, cat);
        return prest;
    }

    public void carregaFicheiroPrestadorServico(Empresa emp) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader("prestadorservicos.txt");
        BufferedReader br = new BufferedReader(fr);

        String linha;
        while ((linha = br.readLine()) != null) {
            PrestadorServico prest = novoPrestadorServico(linha);
            emp.addPrestadorServico(prest);
        }
        br.close();
    }
}