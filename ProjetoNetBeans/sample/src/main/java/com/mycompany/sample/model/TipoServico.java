/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 *
 * @author ZDPessoa
 */
public class TipoServico  {
    
    private String designacao;
    private String classeServico;
    
    public TipoServico(String desig, String cServ){
        
        this.designacao = desig;
        this.classeServico = cServ;
        
    }
    
    public Servico novoServico(String id, String descB, String descC, Double custo, Categoria cat) {
        
    try{
    Class<?> oClass = Class.forName(this.classeServico);
    Class[] argsClasses = new Class[] { String.class, String.class, String.class, Double.class, Categoria.class};
    Constructor constructor = oClass.getConstructor(argsClasses);
    Object[] argsValues = new Object[] { id, descB, descC, custo, cat };
    Servico serv = (Servico) constructor.newInstance(argsValues);
    return serv;
    
    }catch (Exception e){
        System.out.println("Something went wrong...");
        return null;
        }
     
    }
    
    public void getConfigVals() throws IOException {
        
        File fichConfiguracao = new File("config.properties");
        FileReader reader = new FileReader(fichConfiguracao);
        if(reader == null){
            throw new FileNotFoundException("Config Not Found");
        }
        Properties props = new Properties();
        props.load(reader); 
        
    }
    
    public List<TipoServico> criaTiposServicosSuportados(Properties props) {
    
    List<TipoServico> listTipos = new ArrayList<TipoServico>();
    // Conhecer quantos TipoServico são suportados
    String qtdTipos = props.getProperty("Empresa.QuantidadeTiposServicoSuportados");
    int qtd = Integer.parseInt(qtdTipos);
    // Por cada tipo suportado criar a instância respetiva
    for (int i=1; i<=qtd; i++)
    {
    // Conhecer informação (descrição e classe) da instância a criar
        String desc = props.getProperty("Empresa.TipoServico."+ i +".Designacao");
        String classe = props.getProperty("Empresa.TipoServico." + i + ".Classe");
    // Criar a instância
        TipoServico tipoServ = new TipoServico(desc, classe);
    // Adicionar à lista a devolver
        listTipos.add(tipoServ);
    }   
    // Retornar Tipos Suportados
    return listTipos;
}
    
}
