/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

/**
 *
 * @author ZDPessoa
 */
public class Disponibilidade {
    
    private Data dataInicio;
    
    private int horaInicio;
    
    private Data dataFim;
    
    private int horaFim;
    
    private static final Data DATA_POR_OMISSAO = new Data();
    
    private static final int HORA_POR_OMISSAO = 0;
    
    
    public Disponibilidade(Data dataInicio, int horaInicio, Data dataFim, int horaFim){
        
        this.dataFim = dataFim;
        this.dataInicio = dataInicio;
        this.horaFim = horaFim;
        this.horaInicio = horaInicio;
        
    }
    
    public Disponibilidade(){
        
        this.dataFim = DATA_POR_OMISSAO;
        this.dataInicio = DATA_POR_OMISSAO;
        this.horaFim = HORA_POR_OMISSAO;
        this.horaInicio = HORA_POR_OMISSAO;
        
    }
    
}
