/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author ZDPessoa
 */
public class RegistoPrestadorServicos {
    
    private List<PrestadorServico> prestadorServicoList = new ArrayList<PrestadorServico>();
    
    public PrestadorServico getPrestadorServicos(String email){
        
        for(PrestadorServico ps : this.prestadorServicoList ){
            if(ps.getEmail().compareTo(email) == 0)
                return ps;
        }
        
        return null;
    }
    
   
    
    
}
