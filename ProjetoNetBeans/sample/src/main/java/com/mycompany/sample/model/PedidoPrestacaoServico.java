/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sample.model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author ZDPessoa
 */
public class PedidoPrestacaoServico {
    
    private int numero;
    
    private Data Data;
    
    private float total;
    
    private Cliente cliente;
    
    private EnderecoPostal endPostal;
    
    private OutroCusto outroCusto;
    
    private List<DescricaoServicoPedido> DescServList;
    
    private List<PreferenciaHorario> PrefHorList;
    
    
    private static final int NUMERO_POR_OMISSAO = 0;
    
    private static final Data DATA_POR_OMISSAO = new Data();
    
    private static final float TOTAL_POR_OMISSAO = 0;
    
    private static final Cliente CLIENTE_POR_OMISSAO = new Cliente();
    
    private static final EnderecoPostal END_P_POR_OMISSAO = new EnderecoPostal();
    
    
    public PedidoPrestacaoServico (Cliente cliente, EnderecoPostal endPostal){
        
        if ( (cliente == null) || (endPostal == null) )
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = cliente;
        this.endPostal = endPostal;
        this.DescServList = new ArrayList<DescricaoServicoPedido>();
        this.PrefHorList = new ArrayList<PreferenciaHorario>();
        
        
    }
    
    public PedidoPrestacaoServico(){
        
        this.numero = NUMERO_POR_OMISSAO;
        this.Data = DATA_POR_OMISSAO;
        this.total = TOTAL_POR_OMISSAO;
        this.cliente = CLIENTE_POR_OMISSAO;
        this.endPostal = END_P_POR_OMISSAO;
        this.DescServList = new ArrayList<DescricaoServicoPedido>();
        this.PrefHorList = new ArrayList<PreferenciaHorario>();
        
    }
    
    public int getNumero(){
        
        return this.numero;
    }
    
    public void setNumero(int numero){
        
        this.numero = numero;
        
    }
    
    public DescricaoServicoPedido addPedidoServico( Servico serv, String desc, int dur){
        
        DescricaoServicoPedido novoPedido = new DescricaoServicoPedido(serv, desc, dur);
        
        
        return novoPedido;
        
    }
    
    public void addPedidoServico(DescricaoServicoPedido novoPedido){
        
             this.DescServList.add(novoPedido);
             
    }
    
    public void addHorario (Data Data, int hora){
        
        int ordem = countHorarios();
        
        PreferenciaHorario newHorario = new PreferenciaHorario(ordem, Data, hora);
        
        this.PrefHorList.add(newHorario);
        
        
    }

    private int countHorarios(){
        
        int ordem = 0;
        
        for( PreferenciaHorario h : this.PrefHorList)
            ordem = h.getOrdem();
        
        return ordem + 1;
        
    }
    
    public DescricaoServicoPedido getDSP(int i){
        
        for (int j = 0; j < this.DescServList.size() ; j++ ){
            
            if(j == i)
                return this.DescServList.get(j);
            
        }
        
        return null;
        
    }
    
    
}
