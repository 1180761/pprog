/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Autorizacao.AutorizacaoFacade;
import Autorizacao.model.SessaoUtilizador;
import com.mycompany.sample.model.Cliente;
import com.mycompany.sample.model.Empresa;
import com.mycompany.sample.model.EnderecoPostal;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;

import java.net.URL;
import java.util.List;
import java.awt.*;
import java.io.IOException;
import java.util.ResourceBundle;

public class PedidoController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    private SessaoUtilizador sessao;
    private AplicacaoGPSD app;
    private String email;
    private Cliente cli;
    private Empresa emp;
    private List<Cliente> rc;
    private List<EnderecoPostal> lep;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    public void doLogin() {
        String name = username.getText();
        String passw = password.getText();
        AutorizacaoFacade at = new AutorizacaoFacade();
        at.doLogin(name, passw);
        app = AplicacaoGPSD.getInstance();
        sessao = app.getSessaoAtual();
        email = sessao.getEmailUtilizador();
        rc = emp.getRegistoClientes();
        cli = emp.getClienteByEmail(email);
        lep = cli.getEnderecosPostais();

    }
}
