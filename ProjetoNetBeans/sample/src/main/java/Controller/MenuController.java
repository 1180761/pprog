/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.mycompany.sample.model.Guardar;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Guardar guardar = new Guardar();
        try {
            guardar.guardaFicheiros();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @FXML
    public void novoPedido(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("pedido.fxml"));
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.show();
    }
}